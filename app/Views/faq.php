<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>PlayStation Corner | FAQ</title>
    <link rel="stylesheet" href="css/stylefaq.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script>document.getElementsByTagName("html")[0].className += " js";</script>
</head>

<body>
    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-blog set-bg" data-setbg="img/faq/backfaq.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1>FAQ</h1>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Blog Section Begin -->
    <section class="blog spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2"></div>
                <div class="col-lg-8 col-md-8 col-sm-8">
                    <h4 style="text-align: center;">FAQ adalah Frequently Asked Questions. 
                        Halaman ini sebagai referensi sentral untuk menemukan jawaban atas pertanyaan umum. 
                        Jika Anda memiliki pertanyaan yang tidak terdaftar, jangan ragu untuk menghubungi kami.</h4>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2"></div>
            </div>
            <div class="row" style="padding-top: 5%;">
            <div class="col-lg-2 col-md-2 col-sm-2"></div>
                <div class="col-lg-8 col-md-8 col-sm-8">
                    <div class="accordion">
                        <div class="accordion-item">
                        <a>Apakah PSC Memiliki toko fisik? </a>
                        <div class="content">
                            <p>Untuk saat ini kami tidak memiliki toko fisik, tetapi kami berjanji  untuk terus berkomitmen dan berusaha memperluas toko kami ke toko fisik 😊.</p>
                        </div>
                        </div>
                        <div class="accordion-item">
                        <a>Mengapa produk-produk yang kami jual lebih murah dari toko lain? </a>
                        <div class="content">
                            <p>Tentu saja karena kami menjual barang-barang black market dan merugi.</p>
                        </div>
                        </div>
                        <div class="accordion-item">
                        <a>Bagaimana dengan system pembayaran di PSC? </a>
                        <div class="content">
                            <p>Untuk saat ini kami hanya melayani system pembayaran dengan system  Cash On Delivery atau COD.</p>
                        </div>
                        </div>
                        <div class="accordion-item">
                        <a>Apakah produk di PSC bergaransi? </a>
                        <div class="content">
                            <p>Iya untuk console dan aksesoris official dari Playstation bergaransi resmi SONY, sedangkan produk lainnya bergaransi toko PSC</p>
                        </div>
                        </div>
                        <div class="accordion-item">
                        <a>Tapi kan PSC tidak memiliki toko fisik, bagaimana PSC bisa melakukan perbaikan pada produk mereka? </a>
                        <div class="content">
                            <p>Tenang saja, teknisi kami masih memliki rumah atau tempat tinggal sehingga mereka bisa melakukan reparasi di tempat tinggal mereka 😉.</p>
                        </div>
                        <div class="accordion-item">
                        <a>Bagaimana cara mengklaim garansi? </a>
                        <div class="content">
                            <p>Untuk produk yang bergaransi resmi SONY, pelanggan bisa langsung mengunjungi service center SONY di kota masing-masing (bila ada) dan hanya perlu membawa produk, kartu garansi dan nota pembelian yang kami ikut sertakan didalam bungkusan produk saat dikirim. Bila di kota anda tidak memiliki service center SONY, bisa kami bantu klaimkan dengan mengirim barang yang ingin di garansikan ke kami? Dimana? Di Gudang kami. Dan bila ingin mengklaim garansi toko, bisa dikirimkan juga ke alamat Gudang kami.</p>
                        </div>
                        <div class="accordion-item">
                        <a>Apa PSC Menjual Playstation 5?  </a>
                        <div class="content">
                            <p>Untuk saat ini kami hanya menerima pendaftaran melalui whatsapp bagi pelanggan yang ingin membeli PS5, sehingga saat pre order terbuka, kami kembali menginfokan dan mengutamakan pelanggan yang sudah mendaftar terlebih dahulu.</p>
                        </div>
                        <div class="accordion-item">
                        <a>Jenis pengiriman apa saja yang ditawarkan oleh PSC?</a>
                        <div class="content">
                            <p>Banyak! Kami menyediakan pengiriman menggunakan jasa pengiriman seperti JNE, TIKI, J&T hingga siCepat. Serta kami menyediakan pengiriman berupa kargo.</p>
                        </div>
                        </div>
                        <div class="accordion-item">
                        <a>Apa PSC akan menambah produk mereka selain produk Playstation? </a>
                        <div class="content">
                            <p>Kami belum memikirkan hingga sejauh itu, tapi kalau kami menambah produk kami selain produk Playstation, nama kami akan menjadi GameStationCorner HAHAHAHA. Dan juga kami fanboy Playstation.</p>
                        </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2"></div>
            </div>
        </div>
        <div class="row">
            
        </div>
    </section>
    <!-- Blog Section End -->

    <!-- Search Begin -->
    <div class="search-model">
        <div class="h-100 d-flex align-items-center justify-content-center">
            <div class="search-close-switch">+</div>
            <form class="search-model-form">
                <input type="text" id="search-input" placeholder="Search here.....">
            </form>
        </div>
    </div>
    <!-- Search End -->

    <!-- Js Plugins -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery.nicescroll.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/mixitup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/mainfaq.js"></script> 
</body>

</html>