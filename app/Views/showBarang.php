<link rel="stylesheet" href='css/bootstrap.min.css' type="text/css">
<link rel="stylesheet" href='css/font-awesome.min.css' type="text/css">
<link rel="stylesheet" href='css/elegant-icons.css' type="text/css">
<link rel="stylesheet" href="css/magnific-popup.css" type="text/css">
<link rel="stylesheet" href="css/nice-select.css" type="text/css">
<link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
<link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
<link rel="stylesheet" href="css/style.css" type="text/css">
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.nice-select.min.js"></script>
<script src="js/jquery.nicescroll.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/jquery.slicknav.js"></script>
<script src="js/mixitup.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/main.js"></script>
<div class="shop__product__option">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="shop__product__option__left">
            <p><?php 
                $akhir = $page * 12;
                $awal = $akhir - 12 ;
                $awal = ($akhir > 12) ? $awal : 1;
                $akhir = ($jumlah > 12) ? 12 : $jumlah;
                $hasil = ($jumlah <= 12) ? '1-'.$jumlah : $awal."-".$akhir;
                echo $hasil;
            ?> dari <?=$jumlah?> hasil</p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <?php $counter = 0;
    foreach($barang->getResultArray() as $row): ?>
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="product__item">
                <div class="product__item__pic set-bg" data-setbg="img/product/<?=$row['idBarang']?>.jpg">
                    <ul class="product__hover">
                        <!-- <li><a href="#"><img src="img/icon/Love.svg" style="background: none;" width="40px" height="40px" alt=""><span>Whistlist</span></a></li> -->
                        <li><a href="<?=site_url("public/detail?id=").$row['idBarang']?>"><img src="img/icon/Search.svg" style="background: none;" width="40px" height="40px" alt=""><span>Detail</span></a></li>
                    </ul>
                </div>
                <div class="product__item__text">
                    <h6><?=$row['namaBarang']?></h6>
                    <a href="#" class="add-cart" id='<?=$row['idBarang']?>'>+ Add To Cart</a>
                    <div class="rating">
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                    </div>
                    <h5>Rp <?=number_format($row['hargaBarang'],0,",",".")?></h5>
                </div>
            </div>
        </div>
    <?php
        $counter++;
    endforeach;?>
    <?php
        while($counter % 3 != 0){ ?>
            <div class="col-lg-4 col-md-6 col-sm-6">
            </div> <?php
            $counter++;
        }
    ?>
    <div class="col-lg-4 col-md-6 col-sm-6">
    </div>
    <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="product__pagination">
            <!-- LINK NUMBER -->
            <?php
            // Buat query untuk menghitung semua jumlah data
            $jumlah_page = ceil($jumlah / 12); // Hitung jumlah halamanya
            $jumlah_number = 3; // Tentukan jumlah link number sebelum dan sesudah page yang aktif
            $start_number = ($page > $jumlah_number) ? $page - $jumlah_number : 1; // Untuk awal link member
            $end_number = ($page < ($jumlah_page - $jumlah_number)) ? $page + $jumlah_number : $jumlah_page; // Untuk akhir link number
            for ($i = $start_number; $i <= $end_number; $i++) {
                $link_active = ($page == $i) ? 'class="active"' : '';
            ?>
                <a class="paginationKu" id="<?=$i;?>" <?=$link_active?>><?php echo $i; ?></a>
            <?php
            }
            ?>
            <!--<a class="active" href="#">1</a>
            <a href="#">2</a>
            <a href="#">3</a>
            <span>...</span>
            <a href="#">21</a>-->
        </div>
    </div>
<div class="col-lg-4 col-md-6 col-sm-6">
</div>