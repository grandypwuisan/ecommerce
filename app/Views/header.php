<head>
    <meta charset="UTF-8">
    <meta name="description" content="Male_Fashion Template">
    <meta name="keywords" content="Male_Fashion, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- <title>PlayStation Corner | Home</title> -->

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600;700;800;900&display=swap"
    rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Css Styles -->
    <link rel="stylesheet" href='css/wa.css' type="text/css">
    <link rel="stylesheet" href='css/bootstrap.min.css' type="text/css">
    <link rel="stylesheet" href='css/font-awesome.min.css' type="text/css">
    <link rel="stylesheet" href='css/elegant-icons.css' type="text/css">
    <link rel="stylesheet" href="css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery.nicescroll.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/mixitup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
</head>

<body>
    <!-- Whatsapp -->
    <a href="https://api.whatsapp.com/send?phone=6287842231729&text=Halo%21%20PSC%20,%20Saya%20mau%20bertanya" class="float" target="_blank">
    <i class="fa fa-whatsapp my-float"></i>
    </a>

    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Offcanvas Menu Begin -->
    <div class="offcanvas-menu-overlay"></div>
    <div class="offcanvas-menu-wrapper">
        <div class="offcanvas__option">
            <div class="offcanvas__links">
                <?php if(isset(session()->email_login)){ ?>
                    <a href="<?=site_url('public/profile')?>">Account</a>
                <?php } 
                else{ ?>
                    <a href="<?=site_url('public/login')?>">Sign in</a>
                <?php } ?>
                <a href="<?=site_url('public/faq')?>">FAQs</a>
            </div>
        </div>
        <div class="offcanvas__nav__option">
            <!-- <a href="#" class="search-switch"><img src="img/icon/Search.svg" width="30"></a> -->
            <a href="<?=base_url('public/toCart')?>"><img src="img/icon/cart.svg" width="30" alt=""></a>
        </div>
        <div id="mobile-menu-wrap"></div>
    </div>
    <!-- Offcanvas Menu End -->

    <!-- Header Section Begin -->
    <header class="header">
        <div class="header__top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-7">
                        <div class="header__top__left">
                            
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-5">
                        <div class="header__top__right">
                            <div class="header__top__links">
                            <?php if(isset(session()->email_login)){ ?>
                                <a href="<?=site_url('public/profile')?>">Account</a>
                                <a href="<?=site_url('public/logout')?>">Logout</a>
                            <?php } 
                            else{ ?>
                                <a href="<?=site_url('public/login')?>">Sign in</a>
                            <?php } ?>
                                <a href="<?=site_url('public/faq')?>">FAQs</a>
                                <a href="<?=site_url('public/about')?>">About Us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <div class="header__logo">
                        <a href="<?=base_url('/')?>"><img src="img/logops2.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <nav class="header__menu mobile-menu">
                        <ul>
                            <li id="home"><a href="<?php echo site_url("public/home");?>">Home</a></li>
                            <li id="shop"><a href="<?php echo site_url("public/catalog");?>">Shop</a>
                            </li>
                            <li id="preorder"><a href="<?php echo site_url("public/catalog?status=Pre-Order");?>">Upcoming</a></li>
                            <li id="newrelease"><a href="<?php echo site_url("public/catalog?status=NewRelease");?>">New Release</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-lg-3 col-md-3">
                    <div class="header__nav__option">
                        <!-- <a href="#" class="search-switch"><img src="img/icon/search.svg" width="30"></a> -->
                        <a href="<?=base_url('public/toCart')?>"><img src="img/icon/cart.svg" width="30"></a>
                    </div>
                </div>
            </div>
            <div class="canvas__open"><i class="fa fa-bars" style="color: white;"></i></div>
        </div>
    </header>
    <!-- Header Section End -->

     <!-- Search Begin -->
     <div class="search-model">
        <div class="h-100 d-flex align-items-center justify-content-center">
            <div class="search-close-switch">+</div>
            <form class="search-model-form">
                <input type="text" id="search-input" placeholder="Search here.....">
            </form>
        </div>
    </div>
    <!-- Search End -->