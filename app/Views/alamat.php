<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PlayStation Corner | Alamat</title>
    <link rel="stylesheet" href="css/profilecss.css" type="text/css">
</head>
<body>
    <div class="container" style="margin-top: 5%; margin-bottom: 5%;">
        <div class="row">
            <div class="col-md-2">
                <div class="nama">
                    <div class="gambar">
                        <!-- foto -->
                    </div>
                    <h3><?php echo $dataUser['namaUser']; ?></h3>
                    <p>Member</p>
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link" id="v-pills-home-tab"  href="<?=site_url('/profile')?>" role="tab" aria-controls="v-pills-home" aria-selected="true">Profil</a>
                        <a class="nav-link active" id="v-pills-profile-tab"  href="<?=site_url('/alamat')?>" role="tab" aria-controls="v-pills-profile" aria-selected="false">Alamat</a>
                        <a class="nav-link" id="v-pills-messages-tab" href="<?=base_url('public/history')?>" role="tab" aria-controls="v-pills-messages" aria-selected="false">Riwayat Pesanan</a>
                    </div>
                </div>
            </div>
            <div class="col-md-10" style="padding-left: 7%;">
                <h2>Alamat</h2> <br>
                <!-- pesan -->
                <?php
                    $errors = session()->getFlashdata('errors');
                    $error1 = session()->getFlashdata('error1');
                    $success = session()->getFlashdata('sukses');
                    if(!empty($errors)){ ?>
                    <div class="alert alert-danger" role="alert">
                        Whoops! Ada kesalahan saat input data, yaitu:
                        <ul>
                        <?php foreach ($errors as $error) : ?>
                            <li><?= esc($error) ?></li>
                        <?php endforeach ?>
                        </ul>
                    </div>
                    <?php
                    }
                    if(!empty($success)){ ?>
                    <div class="alert alert-success" role="alert">
                        <?=$success?>
                    </div>
                    <?php } 
                    if(!empty($error1)){ ?>
                        <div class="alert alert-danger" role="alert">
                            <?=$error1?>
                        </div>
                    <?php } 
                ?>
                <!-- pesan -->
                <form action="<?=base_url('/public/editalamat')?>" method="POST">
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="hidden" name="email" value="<?=$dataUser['emailUser']?>">
                        <input type="hidden" name="idAlamat" value="<?=$dataAlamat['idAlamat']?>">
                        <input disabled type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="<?=$dataUser['namaUser']?>">
                    </div>
                    <div class="form-group">
                        <label for="kota">Kota</label><span class="btg" style="color: red;"> *</span> 
                        <br><select class="form-control" name="kota" id="kota">
                            <?php
                            foreach($provinsi as $data){
                                if($data->city_id==$dataAlamat['id_kota']){
                                    echo "<option selected value='".$data->city_id."'>".$data->city_name.", ".$data->province."</option>";
                                }
                                else{
                                    echo "<option value='".$data->city_id."'>".$data->city_name.", ".$data->province."</option>";
                                }
                            }
                            ?>
                        </select>
                        <input type="hidden" name="nama_kota" id="nama_kota" value="<?=$dataAlamat['kota']?>">
                        <input type="hidden" name="provinsi" id="provinsi" value="<?=$dataAlamat['provinsi']?>">
                    </div>
                    <div class="form-group">
                        <label for="kecamatan">Kecamatan</label><span class="btg" style="color: red;"> *</span>
                        <input type="text" class="form-control" id="kecamatan" placeholder="kecamatan" name="kecamatan" value="<?=$dataAlamat['kecamatan']?>">
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat</label><span class="btg" style="color: red;"> *</span>
                        <input type="text" class="form-control" id="alamat" placeholder="Alamat" name="alamat" value="<?=$dataAlamat['namaJalan']?>">
                    </div>
                    <div class="form-group">
                        <label for="Kode Pos">Kode Pos</label><span class="btg" style="color: red;"> *</span>
                        <input type="text" class="form-control" id="kodePos" placeholder="Kode Pos" name="kodePos" value="<?=$dataAlamat['kodePos']?>">
                    </div>
                    <div class="form-group">
                        <label for="telp">Nomor Telpon</label><span class="btg" style="color: red;"> *</span>
                        <input type="text" class="form-control" id="telp" placeholder="telp" name="telp" value="<?=$dataAlamat['telpon']?>">
                    </div>
                    <button type="submit" class="btn btnSave" name="btnSave">Save</button>
                </form>
            </div>
        </div>
    </div>
    
  <script>
    $(document).ready(function(){
        $("#kota").change(function(){
            var isi = $("#kota option:selected").text();
            var pecah = isi.split(",");
            $("#nama_kota").val(pecah[0]);
            $("#provinsi").val(pecah[1]);
        });
    // $("#provinsi").change(function(){
    //   $.ajax({
    //     type: "GET", // Method pengiriman data bisa dengan GET atau POST
    //     url: "<?= site_url('public/getCity'); ?>",
    //     data: {id_province : $("#provinsi").val()},
    //     dataType : 'json',
    //     success: function(result){
    //       console.log(result);
    //     },  
    //     error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
    //       alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
    //     }
    //   });
    // });
    $("#kota").select2();
  });
  </script>
  <script>
      $(".mobile-menu").slicknav({
        prependTo: '#mobile-menu-wrap',
        allowParentLinks: true
    });
    $(".canvas__open").on('click', function () {
        $(".offcanvas-menu-wrapper").addClass("active");
        $(".offcanvas-menu-overlay").addClass("active");
    });

    $(".offcanvas-menu-overlay").on('click', function () {
        $(".offcanvas-menu-wrapper").removeClass("active");
        $(".offcanvas-menu-overlay").removeClass("active");
    });
  </script>
    
</body>
</html>