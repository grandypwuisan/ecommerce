<!DOCTYPE html>
<html lang="zxx">

<head>
    <!-- Midtrans -->
    <script type="text/javascript"
            src="https://app.sandbox.midtrans.com/snap/snap.js"
            data-client-key="SB-Mid-client-5nGTHNeVZy_E_-RG"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

    <meta charset="UTF-8">
    <meta name="description" content="Male_Fashion Template">
    <meta name="keywords" content="Male_Fashion, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PlayStation Corner | Cart</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<style>
.glow {
  font-size: 30px;
  color: blue;
  text-align: center;
  -webkit-animation: glow 1s ease-in-out infinite alternate;
  -moz-animation: glow 1s ease-in-out infinite alternate;
  animation: glow 1s ease-in-out infinite alternate;
}

@-webkit-keyframes glow {
  from {
    text-shadow: 0 0 10px #fff, 0 0 12px rgb(206, 206, 206), 0 0 17px #e60073, 0 0 26px #e60073, 0 0 33px #e60073, 0 0 42px #e60073, 0 0 52px #e60073;
  }
  to {
    text-shadow: 0 0 14px #fff, 0 0 16px purple, 0 0 23px purple, 0 0 32px purple 0 0 41px purple, 0 0 51px purple, 0 0 62px purple;
  }
}
.shopping-cart span{
    color: black;
}
.product__cart__item__pic img{
    width : 190px
}
th{
    color: springgreen;
}

</style>
<body>
    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__text">
                        <h4>Keranjang Belanja</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Shopping Cart Section Begin -->
    <section class="shopping-cart spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="shopping__cart__table">
                        <table>
                            <thead>
                                <tr>
                                    <th>Produk</th>
                                    <th>Kuantitas</th>
                                    <th>Total</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach($cart as $row) {
                                ?>
                                <tr id='<?=$row->id?>'>
                                    <td class="product__cart__item">
                                        <div class="product__cart__item__pic">
                                            <img src="img/product/<?=$row->id?>.jpg" alt="">
                                        </div>
                                        <div class="product__cart__item__text">
                                            <h6><?=$row->namaBarang?></h6>
                                            <h5>Status : <?=$row->statusBarang?></h5>
                                        </div>
                                    </td>
                                    <td class="quantity__item">
                                        <div class="quantity">
                                            <?php 
                                            if($row->statusBarang == 'Ready') {
                                            ?>
                                            <div class="pro-qty-2" id='<?=$row->id?>'>
                                                <input type="text" value="<?=$row->qty?>" disabled class='qty-beli'>
                                                <input type ='hidden' value="">
                                            </div>
                                            <?php }?>
                                        </div>
                                    </td>
                                    <td class="cart__price">Rp. <?=number_format($row->harga)?></td>
                                    <td class="cart__close"><i class="fa fa-close" id='<?=$row->id?>'></i></td>
                                </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="continue__btn">
                                <a href="<?=base_url('public/catalog')?>">Lanjut Berbelanja</a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <!-- <div class="continue__btn update__btn">
                                <a href="#"><i class="fa fa-spinner"></i> Memproses Pesanan</a>
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <!-- <div class="cart__discount">
                        <h6 class="glow">Kupon Promo</h6>
                        <input type="text" style="margin-left:10%;" placeholder="Kode Diskon" id='kodeDiskon'>
                        <button class='btn btn-dark'onclick='diskon()'>Gunakan</button>
                    </div> -->
                    <div class="cart__total">
                        <h6>Total Pembayaran</h6>
                        <ul>
                            <li id='subtotal'>Subtotal <span>Rp. <?=number_format($total)?></span></li>
                            <!-- <li id='totalDiskon'>Diskon <span>Rp. 0</span></li> -->
                            <!-- <li id='total'>Total<span>Rp. <?=number_format($total)?></span></li> -->
                        <!-- </ul> -->
                        <?php
                            if(session('alamat')=='tidak'){ ?>
                                <li id='total'>Total<span>Rp. <?=number_format($total)?></span></li>
                                </ul>
                                <a id="almt" href="<?=base_url('public/alamat')?>">Silahkan isi Alamat untuk lanjut</a><br><br>
                            <?php
                            }
                            else{
                                //tampilkan ongkir
                                echo "<li id='ongkir'>Ongkir <span>Rp ".number_format(session('ongkir'))."</span></li>";
                            ?>
                            <li id='total'>Total<span>Rp. <?=number_format($total + session('ongkir'))?></span></li></ul>
                            <a href="#" id="btn-checkout" class="primary-btn">Lanjut Ke Checkout</a>
                            <?php
                            }
                        ?>
                        <form id="payment-form" method="post" action="<?=site_url()?>public/finish">
                            <input type="hidden" name="result_type" id="result-type" value=""></div>
                            <input type="hidden" name="result_data" id="result-data" value=""></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Shopping Cart Section End -->

    <!-- Search Begin -->
    <div class="search-model">
        <div class="h-100 d-flex align-items-center justify-content-center">
            <div class="search-close-switch">+</div>
            <form class="search-model-form">
                <input type="text" id="search-input" placeholder="Search here.....">
            </form>
        </div>
    </div>
    <!-- Search End -->

    <!-- Js Plugins -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery.nicescroll.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/mixitup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
    <script type='text/javascript'>
    
    //Midtrans
    $('#btn-checkout').click(function (event) {
      event.preventDefault();
      $(this).attr("disabled", "disabled");
    $.ajax({
      url: '<?=site_url()?>public/token',
      cache: false,

      success: function(data) {
        //location = data;
        console.log('token = '+data);
        
        var resultType = document.getElementById('result-type');
        var resultData = document.getElementById('result-data');

        function changeResult(type,data){
          $("#result-type").val(type);
          $("#result-data").val(JSON.stringify(data));
          //resultType.innerHTML = type;
          //resultData.innerHTML = JSON.stringify(data);
        }

        snap.pay(data, {
          
          onSuccess: function(result){
            changeResult('success', result);
            console.log(result.status_message);
            console.log(result);
            $("#payment-form").submit();
          },
          onPending: function(result){
            changeResult('pending', result);
            console.log(result.status_message);
            $("#payment-form").submit();
          },
          onError: function(result){
            changeResult('error', result);
            console.log(result.status_message);
            $("#payment-form").submit();
          }
        });
      }
    });
  });

    $(".fa.fa-close").on('click', function(event){
        //console.log(this.id);
        var idBarang = this.id;
        var id = "#"+ idBarang;
        $.ajax({
            type: "POST",
            url: "<?=site_url('public/hapusCart')?>",
            data: {barang :  idBarang},
            dataType: 'json',
            success: function(data){
                //alert(JSON.stringify(data));
                $(id).remove();
                $('#subtotal span').text('Rp. '+data.subtotal.toLocaleString());
                //$('#totalDiskon span').text('Rp. '+data.diskon.toLocaleString());
                $('#total span').text('Rp. '+(data.total+data.ongkir).toLocaleString());
                $('#ongkir span').text('Rp. '+data.ongkir.toLocaleString());
            },
            error: function(response) {
                alert(response.status);
            }
        });
    });
    // $(".fa.fa-angle-right.inc.qtybtn").on('click', function(event){
    //     alert(this.class);
    // });
    $(".pro-qty-2").on('click', function(event){
        var id = this.id;
        var qtyBaru = $("#"+id+".pro-qty-2 .qty-beli").val();
        if(qtyBaru == 0)
        {
            $.ajax({
                type: "POST",
                url: "<?=site_url('public/hapusCart')?>",
                data: {barang :  id, jumlah : qtyBaru},
                dataType: 'json',
                success: function(data){
                    //alert(JSON.stringify(data));
                    if(data.kode == 400)
                    {
                        $("#"+id+".pro-qty-2 .qty-beli").val(qtyBaru - 1);
                        alert(data.pesan);
                    }
                    else
                    {
                        $('#'+id).remove();
                        $('#subtotal span').text('Rp. '+data.subtotal.toLocaleString());
                        //$('#totalDiskon span').text('Rp. '+data.diskon.toLocaleString());
                        $('#total span').text('Rp. '+data.total.toLocaleString());
                    }
                },
                error: function(response) {
                    alert(response.status);
                }
            });
        }
        else
        {
            $.ajax({
                type: "POST",
                url: "<?=site_url('public/ubahCart')?>",
                data: {barang :  id, jumlah : qtyBaru},
                dataType: 'json',
                success: function(data){
                    alert(data.pesan);
                    $('#subtotal span').text('Rp. '+data.subtotal.toLocaleString());
                    //$('#totalDiskon span').text('Rp. '+data.diskon.toLocaleString());
                    $('#total span').text('Rp. '+(data.total+data.ongkir).toLocaleString());
                    $('#ongkir span').text('Rp '+data.ongkir.toLocaleString());
                    console.log(<?=session('ongkir')?>);
                },
                error: function(response) {
                    alert(response.status);
                }
            });
        }
    });
    function diskon(){
        var kode = $('#kodeDiskon').val();
        $.ajax({
            type: "POST",
            url: "<?=site_url('public/pakaiPromo')?>",
            data: {promo :  kode},
            dataType: 'json',
            success: function(data){
                alert(data.pesan);
                if(data.kode == 200)
                {
                    $('#subtotal span').text('Rp. '+data.subtotal.toLocaleString());
                    //$('#totalDiskon span').text('Rp. '+data.diskon.toLocaleString());
                    $('#total span').text('Rp. '+data.total.toLocaleString());
                }
            },
            error: function(response) {
                alert(response.status);
            }
        });
    }
    </script>
</body>

</html>