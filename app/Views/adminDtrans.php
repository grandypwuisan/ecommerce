<div style='padding:4%'>
<table class='table table-dark'>
    <thead>
        <tr>
            <th>Nama Barang</th>
            <th>Kuantitas</th>
            <th>Harga</th>
            <th>Subtotal</th>
            <th>Berat(gram)</th>
        </tr>
    </thead>
    <tbody>
    <?php 
        foreach($detail as $row) {
    ?>
        <tr>
            <td><?=$row->namaBarang?></td>
            <td><?=$row->kuantitas?></td>
            <td><?=$row->hargaBarang?></td>
            <td><?=$row->subTotal?></td>
            <td><?=$row->beratBarang?></td>
        </tr>
    <?php
        }
    ?>
    </tbody>
</table>
</div>
<script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery.nicescroll.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/mixitup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>