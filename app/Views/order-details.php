<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PlayStation Corner | Details</title>
    <link rel="stylesheet" href="css/profilecss.css" type="text/css">
</head>
<body>
    <div class="container" style="margin-top: 5%; margin-bottom: 5%;">
        <div class="row">
            <div class="col-md-2">
                <div class="nama">
                    <div class="gambar">
                        <!-- foto -->
                    </div>
                    <h3><?php echo $dataUser['namaUser']; ?></h3>
                    <p>Member</p>
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link" id="v-pills-home-tab"  href="<?=site_url('/public/profile')?>" role="tab" aria-controls="v-pills-home" aria-selected="false">Profil</a>
                        <a class="nav-link" id="v-pills-profile-tab"  href="<?=site_url('/public/alamat')?>" role="tab" aria-controls="v-pills-profile" aria-selected="false">Alamat</a>
                        <a class="nav-link active" id="v-pills-messages-tab" href="<?=base_url('public/history')?>" role="tab" aria-controls="v-pills-messages" aria-selected="true">Riwayat Pesanan</a>
                    </div>
                </div>
            </div>
            <div class="col-md-10" style="padding-left: 7%;">
                <h2>History</h2> <br>
                <?php
                if($dtrans != null){ ?>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>PRODUK</th>
                            <th>TOTAL</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        echo "<h5 style='color:gray'> Order #".$htrans["idTrans"]." dipesan pada tanggal ".date('d F Y',strtotime($htrans['tglTransaksi']));
                        echo "<br>Status saat ini ".$htrans['statusPembayaran'];
                        echo "<br>Nomor Pembayaran Virtual Account anda ".$htrans['vakun'];
                        echo "<br><br>";
                        $total = 0;
                            foreach($dtrans as $row){
                                echo "<tr>";
                                echo "<td class='align-middle'>".$row['namaBarang']."&nbsp&nbspx".$row['kuantitas']."</td>";
                                echo "<td class='align-middle'> Rp ".number_format($row['subTotal'], 0, ",", ".")."</td>";
                                echo "</tr>";
                                $total = $total + $row['subTotal'];
                            }
                            echo "<tr style='color:#0ab6f5;'>";
                            echo "<td class='align-middle'>Total  : </td>";
                            echo "<td class='align-middle'> Rp ".number_format($total, 0, ",", ".")."</td>";
                            echo "</tr>";
                    }
                    else{
                        echo "<h3>Belum Ada Transaksi</h3>";
                    }
                    ?>
                    </tbody>
                </table>
                <?php 
                    if($pengiriman == 'sudah'){ ?>
                <h3>Status Pengiriman : <?=$tracking['summary']['status']?></h3>
                <table class='table'>
                    <thead>
                        <tr>
                            <th>Lokasi</th>
                            <th>Keterangan</th>
                            <th>Tanggal</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                        foreach($tracking['history'] as $row => $value) {
                    ?>
                        <tr>
                            <td><?=$value['location']?></td>
                            <td><?=$value['desc']?></td>
                            <td><?=$value['date']?></td>
                        </tr>
                    <?php
                        }
                    ?>
                    </tbody>
                </table>
                    <?php }
                    else {
                     ?>
                    <h3>Status Pengiriman : Belum Terkirim</h3>
                    <?php } ?>
            </div>
        </div>
    </div>
    
<script>

</script>
    
</body>
</html>