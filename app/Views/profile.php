<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PlayStation Corner | Profile</title>
    <link rel="stylesheet" href="css/profilecss.css" type="text/css">
</head>
<body>
    <div class="container" style="margin-top: 5%; margin-bottom: 5%;">
        <div class="row">
            <div class="col-md-2">
                <div class="nama">
                    <div class="gambar">
                        <!-- foto -->
                    </div>
                    <h3><?php echo $namaUser; ?></h3>
                    <p>Member</p>
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link active" id="v-pills-home-tab"  href="<?=base_url('public/profile')?>" role="tab" aria-controls="v-pills-home" aria-selected="true">Profil</a>
                        <a class="nav-link" id="v-pills-profile-tab"  href="<?=base_url('public/alamat')?>" role="tab" aria-controls="v-pills-profile" aria-selected="false">Alamat</a>
                        <a class="nav-link" id="v-pills-messages-tab" href="<?=base_url('public/history')?>" role="tab" aria-controls="v-pills-messages" aria-selected="false">Riwayat Pesanan</a>
                    </div>
                </div>
            </div>
            <div class="col-md-10" style="padding-left: 7%;">
                <h2>Profil</h2> <br>
                <?php
                    $berhasil = session()->getFlashdata('berhasil');
                    $gagal = session()->getFlashdata('gagal');
                    if($gagal!=null){ ?>
                        <div class="alert alert-danger" role="alert">
                            <?= $gagal?>
                        </div>
                    <?php
                    }
                    else if($berhasil!=null){ ?>
                        <div class="alert alert-success" role="alert">
                            <?=$berhasil?>
                        </div>
                    <?php    
                    }
                ?>
                <form action="<?=base_url("/public/editprofile")?>" method="POST">
                    <div class="form-group">
                        <label for="nama">Nama:</label>
                        <input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="<?=$namaUser?>">
                    </div>
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="email" class="form-control" id="email" placeholder="Email" name="email" value="<?=$emailUser?>">
                    </div>
                    <br><br><h3>Ganti Password</h3><br>
                    <?php
                    $berhasil2 = session()->getFlashdata('berhasil2');
                    $gagal2 = session()->getFlashdata('gagal2');
                    if($gagal2!=null){ ?>
                        <div class="alert alert-danger" role="alert">
                            <?= $gagal2?>
                        </div>
                    <?php
                    }
                    else if($berhasil2!=null){ ?>
                        <div class="alert alert-success" role="alert">
                            <?=$berhasil2?>
                        </div>
                    <?php    
                    }
                    ?>
                    <div class="form-group">
                        <label for="oldpass">Password Lama:</label>
                        <input type="password" class="form-control" id="oldpass" placeholder="Password Lama" name="oldpass">
                    </div>
                    <div class="form-group">
                        <label for="npass">Password Baru:</label>
                        <input type="password" class="form-control" id="npass" placeholder="Password Baru" name="npass">
                    </div>
                    <div class="form-group">
                        <label for="oldpass">Konfirmasi Password Baru:</label>
                        <input type="password" class="form-control" id="cpass" placeholder="Konfirmasi Password Baru" name="cpass">
                    </div>
                    
                    <button type="submit" class="btn btnSave" name="btnSave">Save</button>
                </form>
            </div>
        </div>
    </div>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery.nicescroll.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/mixitup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
</body>
</html>