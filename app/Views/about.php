<head><title>PlayStation Corner | About Us</title></head>
<script>
    $(document).ready(function () {
        $('nav a').parents('li,ul').removeClass('active');
    });
</script>
    <!-- About Section Begin -->
    <section class="about spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12" style="width: 100%;">
                    <div class="about__pic">
                        <img src="img/about/about-us.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="about__item">
                        <h4>Who We Are ?</h4>
                        <p>Kami adalah toko online yang menjual berbagai barang yang berhubungan dengan
                            PlayStation. Kami mulai beruperasi sejak tahun 2020
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="about__item">
                        <h4>What We Do ?</h4>
                        <p>PS Corner menjual berbagai jenis barang-barang yang berhubungan dengan PlayStation,
                            mulai dari console, games, prepaid card, aksesoris, dan mainan.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="about__item">
                        <h4>Why Choose Us</h4>
                        <p>Kami memiliki koleksi produk PlayStation yang sangat lengkap.
                            Kami selalu berupaya memberikan yang terbaik dari segi harga, pelayanan, dan selalu up to date dalam
                            dunia PlayStation.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- About Section End -->

    <!-- Testimonial Section Begin -->
    <section class="testimonial">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 p-0" style="background-image: url('img/about/bg2.jpg');background-size: cover;">
                    <div class="testimonial__text">
                        <span class="icon_quotations"></span>
                        <p>“Belanja di sini benar-benar membuat hati senang. Harganya mmurah, pelayanan sangat memuaskan,
                            dan barang-barangnya sangat keren dan memberi semangat menjalani hari-hari”
                        </p>
                        <div class="testimonial__author">
                            <div class="testimonial__author__pic">
                                <img src="img/about/testimonial-author.jpg" alt="">
                            </div>
                            <div class="testimonial__author__text">
                                <h5>Ruka Lontara' </h5>
                                <p>Cafe owner</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 p-0">
                    <div class="testimonial__pic set-bg" data-setbg="img/about/testimonial-pic.jpg"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- Testimonial Section End -->

    <!-- Search Begin -->
    <div class="search-model">
        <div class="h-100 d-flex align-items-center justify-content-center">
            <div class="search-close-switch">+</div>
            <form class="search-model-form">
                <input type="text" id="search-input" placeholder="Search here.....">
            </form>
        </div>
    </div>
    <!-- Search End -->

    <!-- Js Plugins -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery.nicescroll.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/mixitup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>