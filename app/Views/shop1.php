<head><title>PlayStation Corner | Shop</title>
    <style>
        .kategori:hover{
            cursor: pointer;
        }
        .harga:hover{
            cursor: pointer;
        }
        .genre:hover{
            cursor: pointer;
        }
        .sort:hover{
            cursor: pointer;
        }
    </style>
</head>
<script>
    $(document).ready(function () {
        $('nav a').parents('li,ul').removeClass('active');
        // $('#shop').addClass('active');
    });
</script>
<!-- Breadcrumb Section Begin -->
<input type="hidden" name="<?= csrf_token() ?>" value="<?= csrf_hash() ?>" />
    <section class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__text">
                        <h4>Shop</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Shop Section Begin -->
    <section class="shop spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="shop__sidebar">
                        <div class="shop__sidebar__search">
                            <input class="form-control" id="searchKu" placeholder="Search..." aria-label="Search">
                        </div>
                        <div class="shop__sidebar__accordion">
                            <div class="accordion" id="accordionExample">
                                <div class="card">
                                    <div class="card-heading">
                                        <a data-toggle="collapse" data-target="#collapseOne">Kategori</a>
                                    </div>
                                    <div id="collapseOne" class="collapse show" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="shop__sidebar__categories">
                                                <ul class="nice-scroll">
                                                <?php 
                                                    foreach($kategori->getResultArray() as $row): ?>
                                                        <li><a class="kategori" id="<?=$row["kategoriBarang"]?>"><?=$row["kategoriBarang"]?></a></li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-heading">
                                        <a data-toggle="collapse" data-target="#collapseThree">Harga</a>
                                    </div>
                                    <div id="collapseThree" class="collapse show" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="shop__sidebar__price">
                                                <ul>
                                                    <li><a class="harga" id="harga1" name="0,100000">Rp 0 - Rp 100.000</a></li>
                                                    <li><a class="harga" id="harga2" name="100000,250000">Rp 100.000 - Rp 250.000</a></li>
                                                    <li><a class="harga" id="harga3" name="250000,500000">Rp 250.000 - Rp 500.000</a></li>
                                                    <li><a class="harga" id="harga4" name="500000,1000000">Rp 500.000 - Rp 1.000.000</a></li>
                                                    <li><a class="harga" id="harga5" name="1000000">Rp 1.000.000+</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-heading">
                                        <a data-toggle="collapse" data-target="#collapseFour">Genre Kaset</a>
                                    </div>
                                    <div id="collapseFour" class="collapse show" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="shop__sidebar__categories">
                                                <ul>
                                                    <li><a class="genre" id="Action">Action</a></li>
                                                    <li><a class="genre" id="Adventure">Adventure</a></li>
                                                    <li><a class="genre" id="Puzzle">Puzzle</a></li>
                                                    <li><a class="genre" id="Racing">Racing</a></li>
                                                    <li><a class="genre" id="Sport">Sport</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-heading">
                                        <a data-toggle="collapse" data-target="#collapseFour">Sort By Price</a>
                                    </div>
                                    <div id="collapseFour" class="collapse show" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="shop__sidebar__categories">
                                                <ul>
                                                    <li><a class="sort" id="asc" name="asc">Low To High</a></li>
                                                    <li><a class="sort" id="desc" name="desc">High To Low</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--<div class="card">
                                    <div class="card-heading">
                                        <a data-toggle="collapse" data-target="#collapseSix">Tag</a>
                                    </div>
                                    <div id="collapseSix" class="collapse show" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="shop__sidebar__tags">
                                                <a href="#">Playstation 1</a>
                                                <a href="#">Playstation 2</a>
                                                <a href="#">Playstation 3</a>
                                                <a href="#">Playstation 4</a>
                                                <a href="#">Playstation 5</a>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        </div>
                        <div id="isi">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Shop Section End -->

    

    <!-- Search Begin -->
    <div class="search-model">
        <div class="h-100 d-flex align-items-center justify-content-center">
            <div class="search-close-switch">+</div>
            <form class="search-model-form">
                <input type="text" id="search-input" placeholder="Search here.....">
            </form>
        </div>
    </div>
    <!-- Search End -->

    <!-- Js Plugins -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery.nicescroll.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/mixitup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function (){
            var kategoriBarang = "";
            var minValBarang = "";
            var maxValBarang = "";
            var ctrHarga = "";
            var genreBarang = "";
            var pageKu = 1;
            var search = "";
            var statusBarang = "<?=$status?>";
            var sortBy = "";
            $.ajax({
                type: "POST",
                url: "<?=site_url('public/loadBarang')?>",
                data: { status :  statusBarang},
                success: function(data){
                    $('#isi').html(data);
                },
                error: function(response){
                    alert(response.status);
                },
                async: false
            });
            $(".sort").on('click', function(event){
                //console.log(this.id);
                var simpanan = "";
                if(sortBy == this.id){
                    $('#'+sortBy).css("background-color","white");
                    sortBy = "";
                }else{
                    simpanan = sortBy;
                    if(sortBy == "asc"){
                        sortBy = "desc";
                    }else if(sortBy == "desc"){
                        sortBy = "asc";
                    }else{
                        sortBy = this.id;
                    }
                    $('#'+sortBy).css("background-color","red");
                    if(simpanan != ""){
                        $('#'+simpanan).css("background-color","white");
                    }
                }
                pageKu = 1;
                $.ajax({
                    type: "POST",
                    url: "<?=site_url('public/loadBarang')?>",
                    data: { kategori :  kategoriBarang , minVal : minValBarang, maxVal : maxValBarang, genre : genreBarang ,cari : search, status:statusBarang, sort: sortBy},
                    success: function(data){
                        $('#isi').html(data);
                    },
                    error: function(response) {
                        alert(response.status);
                    }
                });
            });
            $(".kategori").on('click', function(event){
                //console.log(this.id);
                var simpanan = "";
                if(kategoriBarang == this.id){
                    $('#'+kategoriBarang).css("background-color","white");
                    kategoriBarang = "";
                }else{
                    simpanan = kategoriBarang;
                    kategoriBarang = this.id;
                    $('#'+kategoriBarang).css("background-color","red");
                    if(simpanan != ""){
                        $('#'+simpanan).css("background-color","white");
                    }
                }
                pageKu = 1;
                $.ajax({
                    type: "POST",
                    url: "<?=site_url('public/loadBarang')?>",
                    data: { kategori :  kategoriBarang , minVal : minValBarang, maxVal : maxValBarang, genre : genreBarang ,cari : search, status:statusBarang, sort: sortBy},
                    success: function(data){
                        $('#isi').html(data);
                    },
                    error: function(response) {
                        alert(response.status);
                    },
                    async : false
                });
            });
            $(".harga").on('click', function(event){
                //console.log(this.id);
                hargaBarang = this.name;
                harga = hargaBarang.split(",");
                var simpanan = "";
                if(minValBarang == harga[0]){
                    $('#'+ctrHarga).css("background-color","white");
                    ctrHarga = "";
                    minValBarang = "";
                    maxValBarang = "";
                }else{
                    simpanan = ctrHarga;
                    ctrHarga = this.id;
                    $('#'+ctrHarga).css("background-color","red");
                    if(simpanan != ""){
                        $('#'+simpanan).css("background-color","white");
                    }
                    minValBarang = harga[0];
                    maxValBarang = harga[1];
                }
                pageKu = 1;
                $.ajax({
                    type: "POST",
                    url: "<?=site_url('public/loadBarang')?>",
                    data: { kategori :  kategoriBarang , minVal : minValBarang, maxVal : maxValBarang, genre : genreBarang, cari : search, status:statusBarang, sort: sortBy },
                    success: function(data){
                        $('#isi').html(data);
                    },
                    error: function(response) {
                        alert(response.status);
                    },
                    async : false
                });
            });
            $(".genre").on('click', function(event){
                //console.log(this.id);
                var simpanan = "";
                if(genreBarang == this.id){
                    $('#'+genreBarang).css("background-color","white");
                    genreBarang = "";
                }else{
                    simpanan = genreBarang;
                    genreBarang = this.id;
                    $('#'+genreBarang).css("background-color","red");
                    if(simpanan != ""){
                        $('#'+simpanan).css("background-color","white");
                    }
                }
                pageKu = 1;
                $.ajax({
                    type: "POST",
                    url: "<?=site_url('public/loadBarang')?>",
                    data: { kategori :  kategoriBarang , minVal : minValBarang, maxVal : maxValBarang, genre : genreBarang,cari : search, status:statusBarang, sort: sortBy  },
                    success: function(data){
                        $('#isi').html(data);
                    },
                    error: function(response) {
                        alert(response.status);
                    },
                    async : false
                });
            });
            $("#isi").on("click",".paginationKu", function(){
                pageKu = this.id;
                $.ajax({
                    type: "POST",
                    url: "<?=site_url('public/loadBarang')?>",
                    data: { kategori :  kategoriBarang , minVal : minValBarang, maxVal : maxValBarang, genre : genreBarang, page : pageKu,cari : search, status:statusBarang, sort: sortBy },
                    success: function(data){
                        $('#isi').html("");
                        $('#isi').html(data);
                    },
                    error: function(response) {
                        alert(response.status);
                    },
                    async : false
                });
            });
            $("#isi").on("click",".paginationKu", function(){
                pageKu = this.id;
                $.ajax({
                    type: "POST",
                    url: "<?=site_url('public/tombolCart')?>",
                    data: { barang :  pageKu , jumlah : 1},
                    success: function(data){
                        alert(JSON.stringify(data));
                    },
                    error: function(response) {
                        alert(response.status);
                    },
                    async : false
                });
            });
            $("#isi").on('click',".add-cart", function(event){
                var idBarang = this.id;
                $.ajax({
                    type: "POST",
                    url: "<?=site_url('public/tombolCart')?>",
                    dataType: 'json',
                    data: { barang :  idBarang , jumlah : 1},
                    success: function(data){
                        alert(data.pesan);
                    },
                    error: function(response) {
                        alert(response.status);
                    }
                });
            });
            $('#searchKu').keypress(function(e){
                if (e.which == 13) {
                    search = $('#searchKu').val();
                    $.ajax({
                        type: "POST",
                        url: "<?=site_url('public/loadBarang')?>",
                        data: { kategori :  kategoriBarang , minVal : minValBarang, maxVal : maxValBarang, genre : genreBarang, page : pageKu,cari : search },
                        success: function(data){
                            $('#isi').html("");
                            $('#isi').html(data);
                        },
                        error: function(response) {
                            alert(response.status);
                        },
                        async : false
                    });
                }
            });
        });
    </script>
</body>

</html>