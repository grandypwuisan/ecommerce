<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PlayStation Corner | Master</title>

</head>
<body>
    
    <div class="container" id="container1">
    <?php
        $attributes = ['class' => 'classForm', 'id' => 'myform'];
        echo form_open_multipart('public/insertBarang', $attributes);
    ?>
        <h1>Master Barang</h1>
        <div class="form-group">
            <label for="nama">Nama Barang :</label>
            <input type="text" class="form-control" name="nama">
        </div>
        <div class="form-group">
            <label for="harga">Harga Barang :</label>
            <input type="number" class="form-control" name="harga">
        </div>
        <div class="form-group">
            <label for="deskripsi">Deskripsi Barang :</label>
            <input type="text" class="form-control" name="deskripsi">
        </div>
        <div class="form-group">
            <label for="stok">Stok Barang :</label>
            <input type="number" class="form-control" name="stok">
        </div>
        <div class="form-group">
            <label for="berat">Berat Barang :</label>
            <input type="number" class="form-control" name="berat">
        </div>
        <div class="form-group">
            <label for="status">Status Barang :</label>
            <select class="form-control" name="status">
                <option>Ready</option>
                <option>Pre Order</option>
                <option>Out of Stock</option>
            </select>
        </div>
        <div class="form-group">
            <label for="kategori">Kategori Barang :</label>
            <div class="form-check">
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="kategori[]" value="Accessories">
                    <label class="form-check-label">Accessories</label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="kategori[]" value="Console">
                    <label class="form-check-label">Console</label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="kategori[]" value="Games">
                    <label class="form-check-label">Games</label>
                    </div>
            </div>
        </div> <br>
        <div class="form-group">
            <label for="kategori">Genre :</label>
            <div class="form-check">
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="genre[]"  value="action">
                    <label class="form-check-label">Action</label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="genre[]" value="adventure">
                    <label class="form-check-label">Adventure</label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="genre[]" value="puzzle">
                    <label class="form-check-label">Puzzle</label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="genre[]" value="rpg">
                    <label class="form-check-label">RPG</label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="genre[]" value="family">
                    <label class="form-check-label">Family</label>
                    </div>
            </div>
            <div class="form-check">
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="genre[]" value="racing">
                    <label class="form-check-label">Racing</label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="genre[]" value="sport">
                    <label class="form-check-label">Sports</label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="genre[]" value="horror">
                    <label class="form-check-label">Horror</label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="genre[]" value="fighting">
                    <label class="form-check-label">Fighting</label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="genre[]" value="shooter">
                    <label class="form-check-label">Accessories</label>
                    </div>
            </div>
        </div> <br>
        <div class="form-group">
            <label for="stok">Tanggal Rilis :</label>
            <input type="date" class="form-control" name="tglrilis">
        </div>
        <div class="form-group">
            <label for="gambar">Masukkan Gambar</label>
            <input type="file" class="form-control-file" name="gambar">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    </div>
</body>
</html>