<div style='padding:4%'>
<table class='table table-dark'>
    <thead>
        <tr>
            <th>ID Trans</th>
            <th>Email Pelanggan</th>
            <th>Nama Pelanggan</th>
            <th>Alamat Kirim</th>
            <th>Telfon</th>
            <th>Resi</th>
            <th>#</th>
        </tr>
    </thead>
    <tbody>
    <?php 
        $ctr = 0;
        foreach($htrans as $row) {
    ?>
        <tr id = '<?=$row->id?>'>
            <td id='id<?=$ctr?>'><a href='<?=site_url('public/detailAdminPesanan?id='.$row->id)?>'><?=$row->id?></a></td>
            <td><?=$row->email?></td>
            <td><?=$row->nama?></td>
            <td><?=$row->alamat?></td>
            <td><?=$row->noTelpon?></td>
            <td><input type='text' name='nomorResi<?=$ctr?>'></td>
            <td><button onclick='resi(<?=$ctr?>)'>Masukkan Resi</button></td>
        </tr>
    <?php
        $ctr++;
        }
    ?>
    </tbody>
</table>
</div>
<script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery.nicescroll.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/mixitup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function (){
        });
        function resi(id)
        {
            var idT = $('#id'+ id.toString()).text();
            var nores = $("input[name=nomorResi"+id.toString()+"]").val();
            var r = confirm("Yakin memasukkan resi " + nores + " untuk transaksi "+idT);
            if(r == true){
                $.ajax({
                    type: "POST",
                    url: "<?=site_url('public/updateResi')?>",
                    dataType: 'json',
                    data: { id : idT, resi : nores},
                    success: function(data){
                        alert(data.pesan);
                        $('#'+idT).remove();
                    },
                    error: function(response) {
                        alert(response.status);
                    }
                });
            }
        }
    </script>