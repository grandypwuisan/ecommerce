<div class="editbrg">
    <div class='editbrg1'>
            <div class="search">
            <div class='row'>
                <div class='col-md-2'>
                    <input type='text' class="form-control" id='nama-pencarian'>
                </div>
                <div class='col-md-2'>
                    <button class="btn btn-light" onclick='cari()'>cari</button>
                </div>
            </div>
        </div>
        <p id='kat'>Kategori : </p>
        <button class = 'btn btn-light'onclick='setJadi(1)'>Aksesoris</button>
        <button class = 'btn btn-light' onclick='setJadi(2)'>Game</button>
        <button class = 'btn btn-light' onclick='setJadi(3)'>Console</button><br><br><br>
        Harga : <input type='number' id='batas-bawah' value='0'> ----- > <input type='number' id='batas-atas' value='0'><br>
        <button onclick='hilangkan(1)'>hilangkan nama</button><button onclick='hilangkan(2)'>hilangkan kategori</button><button onclick='hilangkan(3)' id='btn-angka'>FIlter Angka : Off</button>
    </div>

<div class='table-responsive'>
    <table border = "1" class='table table-dark' >
    <thead>
        <th>Nama Barang</th> 
        <th>Harga</th>
        <th>Deskripsi</th>
        <th>Stok</th>
        <th>Kategori</th>
        <th>Genre</th>
        <th>Status</th>
        <th>Berat</th>
        <th>Foto</th>
        <th>Action</th>
    </thead>

    <tbody id='tbody'>
    <?php
        $ctr = 0;
        foreach($barang -> getResultArray() as $row)
        {
            echo "<tr>";
                echo "<input type='hidden' value='".$row['idBarang']."' name='id$ctr' disabled>";
                echo "<td>"."<input type='text' value='".$row['namaBarang']."' name='nama$ctr'>"."</td>";
                echo "<td>"."<input type='number' value='".$row['hargaBarang']."' name='harga$ctr'>"."</td>";
                echo "<td>"."<input type='text' value='".$row['deskripsiBarang']."' name='deskripsi$ctr'>"."</td>";
                echo "<td>"."<input type='number' value='".$row['stokBarang']."' name='stok$ctr'>"."</td>";
                echo "<td>"."<input type='text' value='".$row['kategoriBarang']."' name='kategori$ctr'>"."</td>";
                echo "<td>"."<input type='text' value='".$row['genre']."' name='genre$ctr'>"."</td>";
                echo "<td>"."<input type='text' value='".$row['statusBarang']."' name='status$ctr'>"."</td>";
                echo "<td>"."<input type='text' value='".$row['beratBarang']."' name='berat$ctr'>"."</td>";
                echo "<td>"."<img src='img/product/".$row['idBarang'].".jpg' width='100' height='100' name='gambar$ctr'>"."</td>";
                echo "<td>"."<button class='btn btn-light' type='button' onclick='tes($ctr)'>Update</button></td>";
            echo "</tr>";
            $ctr++;
        }
    ?>
    </tbody>
</div>
</div>

<script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery.nicescroll.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/mixitup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script type="text/javascript">
        var kategori = "";
        var nama = "";
        $(document).ready(function (){
            
        });
        function hilangkan(angka)
        {
            if(angka == 1)
            {
                $('#nama-pencarian').val("");
                nama = "";
            }
            else if(angka == 2)
            {
                $('#kat').html("Kategori : ");
                kategori = "";
            }
            else{
                $('#batas-bawah').val(0);
                $('#batas-atas').val(0);
                if($('#btn-angka').html() == "FIlter Angka : On") $('#btn-angka').html("FIlter Angka : Off");
                else $('#btn-angka').html("FIlter Angka : On");
            }
        }
        function setJadi(angka){
            if(angka == 1) kategori = "Accessories";
            else if(angka == 2)kategori = 'Games';
            else kategori = 'Console';
            $('#kat').html("Kategori : "+kategori);
        }
        function cari(){
            var min = 0;
            var max = 99999999;
            if($('#btn-angka').html() == "FIlter Angka : On")
            {
                min = $('#batas-bawah').val();
                max = $('#batas-atas').val();
            }
            $.ajax({
              type: "POST",
              url: "<?=site_url('public/cariBarang')?>",
              headers: {'X-Requested-With': 'XMLHttpRequest'},
              data: {jenis : kategori, nama : $('#nama-pencarian').val(), bawah : min,atas : max},
              success: function(data){
                $('#tbody').html("");
                $('#tbody').html(data);
              },
              error: function(response) {
                  alert(response.status);
              }
          });
        }
        function tes(angka){
          var id = $("input[name=id"+angka+"]").val();
          var nama1 = $("input[name=nama"+angka+"]").val();
          var harga1 = $("input[name=harga"+angka+"]").val();
          var deskripsi = $("input[name=deskripsi"+angka+"]").val();
          var stok1 = $("input[name=stok"+angka+"]").val();
          var kategori = $("input[name=kategori"+angka+"]").val();
          var genre1 = $("input[name=genre"+angka+"]").val();
          //var tgl = $("input[name=tgl"+angka+"]").val();
          var stat = $("input[name=status"+angka+"]").val();
          var berat1 = $("input[name=berat"+angka+"]").val();
        //   console.log(nama1);
        //   console.log(harga1);
        //   console.log(deskripsi);
        //   console.log(stok1);
        //   console.log(kategori);
        //   console.log(genre1);
        //   console.log(tgl);
        //   console.log(stat);
          $.ajax({
              type: "POST",
              url: "<?=site_url('public/updateBarang')?>",
              headers: {'X-Requested-With': 'XMLHttpRequest'},
              dataType: 'json',
              data: {barang :  id , nama : nama1, harga: harga1, desk : deskripsi, stok : stok1, kat : kategori, genre: genre1, status : stat,berat:berat1},
              success: function(data){
                  alert("sukses");
              },
              error: function(response) {
                  alert(response.status);
              }
          });
        }
    </script>
