<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PlayStation Corner | History</title>
    <link rel="stylesheet" href="css/profilecss.css" type="text/css">
</head>
<body>
    <div class="container" style="margin-top: 5%; margin-bottom: 5%;">
        <div class="row">
            <div class="col-md-2">
                <div class="nama">
                    <div class="gambar">
                        <!-- foto -->
                    </div>
                    <h3><?php echo $dataUser['namaUser']; ?></h3>
                    <p>Member</p>
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link" id="v-pills-home-tab"  href="<?=site_url('/public/profile')?>" role="tab" aria-controls="v-pills-home" aria-selected="false">Profil</a>
                        <a class="nav-link" id="v-pills-profile-tab"  href="<?=site_url('/public/alamat')?>" role="tab" aria-controls="v-pills-profile" aria-selected="false">Alamat</a>
                        <a class="nav-link active" id="v-pills-messages-tab" href="<?=base_url('public/history')?>" role="tab" aria-controls="v-pills-messages" aria-selected="true">Riwayat Pesanan</a>
                    </div>
                </div>
            </div>
            <div class="col-md-10" style="padding-left: 7%;">
                <h2>History</h2> <br>
                <div class="table-responsive">
                    <?php
                    if($htrans != null){ ?>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>ORDER</th>
                                <th>TANGGAL</th>
                                <th>STATUS</th>
                                <th>TOTAL</th>
                                <th>ACTION</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                                foreach($htrans as $row){
                                    echo "<tr>";
                                    echo "<td class='align-middle'>".$row['idTrans']."</td>";
                                    echo "<td class='align-middle'>".$row['tglPemesanan']."</td>";
                                    echo "<td class='align-middle'>".$row['statusPembayaran']."</td>";
                                    echo "<td class='align-middle'>".$row['totalPembayaran']."</td>";
                                    $url = base_url()."/public/orderdetails?id=".$row["idTrans"];
                                    echo "<td class='align-middle'>".'<a href= '.$url.' class="btn btn-info" role="button">Detail</a>'."</td>";
                                    echo "</tr>";
                                }
                        }
                        else{
                            echo "<h3>Belum Ada Transaksi</h3>";
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
<script>

</script>
    
</body>
</html>