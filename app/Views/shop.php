<head><title>PlayStation Corner | Shop</title></head>
<script>
    $(document).ready(function () {
        $('nav a').parents('li,ul').removeClass('active');
        $('#shop').addClass('active');
    });
</script>
<!-- Breadcrumb Section Begin -->
<input type="hidden" name="<?= csrf_token() ?>" value="<?= csrf_hash() ?>" />
    <section class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__text">
                        <h4>Shop</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Shop Section Begin -->
    <section class="shop spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="shop__sidebar">
                        <div class="shop__sidebar__search">
                            <form method="GET" action="<?= site_url('/catalog') ?>">
                                <input type="text" name="cari" placeholder="Search...">
                                <button type="submit"><span class="icon_search"></span></button>
                            </form>
                        </div>
                        <div class="shop__sidebar__accordion">
                            <div class="accordion" id="accordionExample">
                                <div class="card">
                                    <div class="card-heading">
                                        <a data-toggle="collapse" data-target="#collapseOne">Kategori</a>
                                    </div>
                                    <div id="collapseOne" class="collapse show" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="shop__sidebar__categories">
                                                <ul class="nice-scroll">
                                                <?php 
                                                    foreach($kategori->getResultArray() as $row): ?>
                                                        <?php
                                                            $style = "";
                                                            if(isset($_GET['cari']) || isset($_GET['minVal']) || isset($_GET['genre'])){
                                                                $link = site_url("$_SERVER[REQUEST_URI]"."&kategori=".$row["kategoriBarang"]);
                                                            }else{
                                                                $link = site_url("/catalog?kategori=".$row["kategoriBarang"]);
                                                            }
                                                            if(isset($_GET['kategori']) && $_GET['kategori'] == $row['kategoriBarang']){
                                                                $style="background-color: red";//ini yang diubah klau mau ubh style kalau dipencet
                                                                $url = "$_SERVER[REQUEST_URI]";
                                                                $key = "kategori";
                                                                $link = preg_replace('~(\?|&)'.$key.'=[^&]*~', '${1}', $url);
                                                            }
                                                        ?>
                                                        <li><a href='<?= $link; ?>' style='<?= $style; ?>'><?=$row["kategoriBarang"]?></a></li>
                                                    <?php endforeach;
                                                    $style = "background-color";//ini yang diubah klau mau ubh style kalau dipencet
                                                ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-heading">
                                        <a data-toggle="collapse" data-target="#collapseThree">Harga</a>
                                    </div>
                                    <div id="collapseThree" class="collapse show" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="shop__sidebar__price">
                                                <ul>
                                                <?php
                                                    $style = "background-color: red";
                                                    if((isset($_GET['cari']) || isset($_GET['kategori']) || isset($_GET['genre'])) && isset($_GET['minVal'])){
                                                        if($_GET['minVal'] == 0 && $_GET['maxVal'] == 100000){
                                                            $url = "$_SERVER[REQUEST_URI]";
                                                            $key = "minVal";
                                                            $link = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $url);
                                                            $key = "maxVal";
                                                            $link = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $link);?>
                                                            <li><a href="<?= $link?>" style='<?=$style;?>'>Rp 0 - Rp 100.000</a></li>
                                                        <?php }
                                                        else if($_GET['minVal'] != 0 && $_GET['maxVal'] != 100000){ ?>
                                                            <li><a href="<?= site_url("$_SERVER[REQUEST_URI]"."&minVal=0&maxVal=100000")?>">Rp 0 - Rp 100.000</a></li>
                                                        <?php } ?>
                                                        <?php if($_GET['minVal'] == 100000 && $_GET['maxVal'] == 250000){
                                                            $url = "$_SERVER[REQUEST_URI]";
                                                            $key = "minVal";
                                                            $link = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $url);
                                                            $key = "maxVal";
                                                            $link = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $link);?>
                                                            <li><a href="<?= $link;?>" style='<?=$style;?>'>Rp 100.000 - Rp 250.000</a></li>
                                                        <?php }
                                                        else if($_GET['minVal'] != 100000 && $_GET['maxVal'] != 250000){ ?>
                                                            <li><a href="<?= site_url("$_SERVER[REQUEST_URI]"."&minVal=100000&maxVal=250000")?>">Rp 100.000 - Rp 250.000</a></li>
                                                        <?php } ?>
                                                        <?php if($_GET['minVal'] == 250000 && $_GET['maxVal'] == 500000){
                                                            $url = "$_SERVER[REQUEST_URI]";
                                                            $key = "minVal";
                                                            $link = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $url);
                                                            $key = "maxVal";
                                                            $link = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $link);?>
                                                            <li><a href="<?= $link;?>" style='<?=$style;?>'>Rp 250.000 - Rp 500.000</a></li>
                                                        <?php }
                                                        else if($_GET['minVal'] != 250000 && $_GET['maxVal'] != 500000){ ?>
                                                            <li><a href="<?= site_url("$_SERVER[REQUEST_URI]"."&minVal=250000&maxVal=500000")?>">Rp 250.000 - Rp 500.000</a></li>
                                                        <?php } ?>
                                                        <?php if($_GET['minVal'] == 500000 && $_GET['maxVal'] == 1000000){
                                                            $url = "$_SERVER[REQUEST_URI]";
                                                            $key = "minVal";
                                                            $link = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $url);
                                                            $key = "maxVal";
                                                            $link = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $link);?>
                                                            <li><a href="<?= $link;?>" style='<?=$style;?>'>Rp 500.000 - Rp 1.000.000</a></li>
                                                        <?php }
                                                        else if($_GET['minVal'] != 500000 && $_GET['maxVal'] != 1000000){ ?>
                                                            <li><a href="<?= site_url("$_SERVER[REQUEST_URI]"."&minVal=500000&maxVal=1000000")?>">Rp 500.000 - Rp 1.000.000</a></li>
                                                        <?php } ?>
                                                        <?php if($_GET['minVal'] == 1000000){
                                                            $url = "$_SERVER[REQUEST_URI]";
                                                            $key = "minVal";
                                                            $link = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $url);?>
                                                            <li><a href="<?= $link;?>" style='<?=$style;?>'>Rp 1.000.000+</a></li>
                                                        <?php }
                                                        else if($_GET['minVal'] != 1000000){ ?>
                                                            <li><a href="<?= site_url("$_SERVER[REQUEST_URI]"."&minVal=1000000")?>">Rp 1.000.000+</a></li>
                                                        <?php } 
                                                    }else if((isset($_GET['cari']) || isset($_GET['kategori']) || isset($_GET['genre']) && !isset($_GET['minVal']))){ ?>
                                                        <li><a href="<?= site_url("$_SERVER[REQUEST_URI]"."&minVal=0&maxVal=100000")?>">Rp 0 - Rp 100.000</a></li>
                                                        <li><a href="<?= site_url("$_SERVER[REQUEST_URI]"."&minVal=100000&maxVal=250000")?>">Rp 100.000 - Rp 250.000</a></li>
                                                        <li><a href="<?= site_url("$_SERVER[REQUEST_URI]"."&minVal=250000&maxVal=500000")?>">Rp 250.000 - Rp 500.000</a></li>
                                                        <li><a href="<?= site_url("$_SERVER[REQUEST_URI]"."&minVal=500000&maxVal=1000000")?>">Rp 500.000 - Rp 1.000.000</a></li>
                                                        <li><a href="<?= site_url("$_SERVER[REQUEST_URI]"."&minVal=1000000")?>">Rp 1.000.000+</a></li>
                                                    <?php }
                                                    else if(isset($_GET['minVal'])) {
                                                        if($_GET['minVal'] == 0 && $_GET['maxVal'] == 100000){ 
                                                            $url = "$_SERVER[REQUEST_URI]";
                                                            $key = "minVal";
                                                            $link = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $url);
                                                            $key = "maxVal";
                                                            $link = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $link);?>
                                                            <li><a href="<?= $link;?>" style='<?=$style;?>'>Rp 0 - Rp 100.000</a></li>
                                                        <?php }
                                                        else if($_GET['minVal'] != 0 && $_GET['maxVal'] != 100000){ ?>
                                                            <li><a href="<?= site_url("/catalog?minVal=0&maxVal=100000")?>">Rp 0 - Rp 100.000</a></li>
                                                        <?php } ?>
                                                        <?php if($_GET['minVal'] == 100000 && $_GET['maxVal'] == 250000){
                                                            $url = "$_SERVER[REQUEST_URI]";
                                                            $key = "minVal";
                                                            $link = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $url);
                                                            $key = "maxVal";
                                                            $link = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $link);?>
                                                            <li><a href="<?= $link;?>" style='<?=$style;?>'>Rp 100.000 - Rp 250.000</a></li>
                                                        <?php }
                                                        else if($_GET['minVal'] != 100000 && $_GET['maxVal'] != 250000){ ?>
                                                            <li><a href="<?= site_url("/catalog?minVal=100000&maxVal=250000")?>">Rp 100.000 - Rp 250.000</a></li>
                                                        <?php } ?>
                                                        <?php if($_GET['minVal'] == 250000 && $_GET['maxVal'] == 500000){
                                                            $url = "$_SERVER[REQUEST_URI]";
                                                            $key = "minVal";
                                                            $link = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $url);
                                                            $key = "maxVal";
                                                            $link = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $link);?>
                                                            <li><a href="<?= $link;?>" style='<?=$style;?>'>Rp 250.000 - Rp 500.000</a></li>
                                                        <?php }
                                                        else if($_GET['minVal'] != 250000 && $_GET['maxVal'] != 500000){ ?>
                                                            <li><a href="<?= site_url("/catalog?minVal=250000&maxVal=500000")?>">Rp 250.000 - Rp 500.000</a></li>
                                                        <?php } ?>
                                                        <?php if($_GET['minVal'] == 500000 && $_GET['maxVal'] == 1000000){
                                                            $url = "$_SERVER[REQUEST_URI]";
                                                            $key = "minVal";
                                                            $link = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $url);
                                                            $key = "maxVal";
                                                            $link = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $link);?>
                                                            <li><a href="<?= $link;?>" style='<?=$style;?>'>Rp 500.000 - Rp 1.000.000</a></li>
                                                        <?php }
                                                        else if($_GET['minVal'] != 500000 && $_GET['maxVal'] != 1000000){ ?>
                                                            <li><a href="<?= site_url("/catalog?minVal=500000&maxVal=1000000")?>">Rp 500.000 - Rp 1.000.000</a></li>
                                                        <?php } ?>
                                                        <?php if($_GET['minVal'] == 1000000){
                                                            $url = "$_SERVER[REQUEST_URI]";
                                                            $key = "minVal";
                                                            $link = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $url);?>
                                                            <li><a href="<?= $link;?>" style='<?=$style;?>'>Rp 1.000.000+</a></li>
                                                        <?php }
                                                        else if($_GET['minVal'] != 1000000){ ?>
                                                            <li><a href="<?= site_url("/catalog?minVal=1000000")?>">Rp 1.000.000+</a></li>
                                                        <?php }
                                                    }
                                                    else{ ?>
                                                        <li><a href="<?= site_url("/catalog?minVal=0&maxVal=100000")?>">Rp 0 - Rp 100.000</a></li>
                                                        <li><a href="<?= site_url("/catalog?minVal=100000&maxVal=250000")?>">Rp 100.000 - Rp 250.000</a></li>
                                                        <li><a href="<?= site_url("/catalog?minVal=250000&maxVal=500000")?>">Rp 250.000 - Rp 500.000</a></li>
                                                        <li><a href="<?= site_url("/catalog?minVal=500000&maxVal=1000000")?>">Rp 500.000 - Rp 1.000.000</a></li>
                                                        <li><a href="<?= site_url("/catalog?minVal=1000000")?>">Rp 1.000.000+</a></li>
                                                    <?php }
                                                ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-heading">
                                        <a data-toggle="collapse" data-target="#collapseFour">Genre Kaset</a>
                                    </div>
                                    <div id="collapseFour" class="collapse show" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="shop__sidebar__categories">
                                                <ul>
                                            <?php
                                                if((isset($_GET['cari']) || isset($_GET['minVal']) || isset($_GET['kategori'])) && isset($_GET['genre'])){
                                                    $url = "$_SERVER[REQUEST_URI]";
                                                    $key = "genre";
                                                    if($_GET['genre'] == 'Action'){
                                                        $link = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $url);?>
                                                        <li><a href="<?= $link;?>" style='<?=$style;?>'>Action</a></li>
                                                    <?php }
                                                    else if($_GET['genre'] != 'Action'){ ?>
                                                        <li><a href="<?= site_url("$_SERVER[REQUEST_URI]"."&genre=Action");?>">Action</a></li>
                                                    <?php } ?>
                                                    <?php if($_GET['genre'] == 'Adventure'){
                                                        $link = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $url);?>
                                                        <li><a href="<?= $link;?>" style='<?=$style;?>'>Adventure</a></li>
                                                    <?php }
                                                    else if($_GET['genre'] != 'Adventure'){ ?>
                                                        <li><a href="<?= site_url("$_SERVER[REQUEST_URI]"."&genre=Adventure");?>">Adventure</a></li>
                                                    <?php } ?>
                                                    <?php if($_GET['genre'] == 'Puzzle'){
                                                        $link = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $url);?>
                                                        <li><a href="<?= $link;?>" style='<?=$style;?>'>Puzzle</a></li>
                                                    <?php }
                                                    else if($_GET['genre'] != 'Puzzle'){ ?>
                                                        <li><a href="<?= site_url("$_SERVER[REQUEST_URI]"."&genre=Puzzle");?>">Puzzle</a></li>
                                                    <?php } ?>
                                                    <?php if($_GET['genre'] == 'Racing'){
                                                        $link = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $url);?>
                                                        <li><a href="<?= $link;?>" style='<?=$style;?>'>Racing</a></li>
                                                    <?php }
                                                    else if($_GET['genre'] != 'Racing'){ ?>
                                                        <li><a href="<?= site_url("$_SERVER[REQUEST_URI]"."&genre=Racing");?>">Racing</a></li>
                                                    <?php } ?>
                                                    <?php if($_GET['genre'] == 'Sport'){
                                                        $link = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $url);?>
                                                        <li><a href="<?= $link;?>" style='<?=$style;?>'>Sport</a></li>
                                                    <?php }
                                                    else if($_GET['genre'] != 'Sport'){ ?>
                                                        <li><a href="<?= site_url("$_SERVER[REQUEST_URI]"."&genre=Sport");?>">Sport</a></li>
                                                    <?php } ?>
                                            <?php
                                                }else if((isset($_GET['cari']) || isset($_GET['minVal']) || isset($_GET['kategori'])) && !isset($_GET['genre'])){ ?>
                                                    <li><a href="<?= site_url("$_SERVER[REQUEST_URI]"."&genre=Action");?>">Action</a></li>
                                                    <li><a href="<?= site_url("$_SERVER[REQUEST_URI]"."&genre=Adventure");?>">Adventure</a></li>
                                                    <li><a href="<?= site_url("$_SERVER[REQUEST_URI]"."&genre=Puzzle");?>">Puzzle</a></li>
                                                    <li><a href="<?= site_url("$_SERVER[REQUEST_URI]"."&genre=Racing");?>">Racing</a></li>
                                                    <li><a href="<?= site_url("$_SERVER[REQUEST_URI]"."&genre=Sport");?>">Sport</a></li>
                                                <?php }else if(isset($_GET['genre'])){
                                                    $url = "$_SERVER[REQUEST_URI]";
                                                    $key = "genre";
                                                    if($_GET['genre'] == 'Action'){
                                                        $link = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $url);?>
                                                        <li><a href="<?= $link;?>" style='<?=$style;?>'>Action</a></li>
                                                    <?php }
                                                    else if($_GET['genre'] != 'Action'){ ?>
                                                        <li><a href="<?= site_url("/catalog?genre=Action");?>">Action</a></li>
                                                    <?php } ?>
                                                    <?php if($_GET['genre'] == 'Adventure'){
                                                        $link = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $url);?>
                                                        <li><a href="<?= $link;?>" style='<?=$style;?>'>Adventure</a></li>
                                                    <?php }
                                                    else if($_GET['genre'] != 'Adventure'){ ?>
                                                        <li><a href="<?= site_url("/catalog?genre=Adventure");?>">Adventure</a></li>
                                                    <?php } ?>
                                                    <?php if($_GET['genre'] == 'Puzzle'){
                                                        $link = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $url);?>
                                                        <li><a href="<?= $link;?>" style='<?=$style;?>'>Puzzle</a></li>
                                                    <?php }
                                                    else if($_GET['genre'] != 'Puzzle'){ ?>
                                                        <li><a href="<?= site_url("/catalog?genre=Puzzle");?>">Puzzle</a></li>
                                                    <?php } ?>
                                                    <?php if($_GET['genre'] == 'Racing'){
                                                        $link = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $url);?>
                                                        <li><a href="<?= $link;?>" style='<?=$style;?>'>Racing</a></li>
                                                    <?php }
                                                    else if($_GET['genre'] != 'Racing'){ ?>
                                                        <li><a href="<?= site_url("/catalog?genre=Racing");?>">Racing</a></li>
                                                    <?php } ?>
                                                    <?php if($_GET['genre'] == 'Sport'){
                                                        $link = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $url);?>
                                                        <li><a href="<?= $link;?>" style='<?=$style;?>'>Sport</a></li>
                                                    <?php }
                                                    else if($_GET['genre'] != 'Sport'){ ?>
                                                        <li><a href="<?= site_url("/catalog?genre=Sport");?>">Sport</a></li>
                                                    <?php } 
                                                }else{ ?>
                                                    <li><a href="<?= site_url("/catalog?genre=Action");?>">Action</a></li>
                                                    <li><a href="<?= site_url("/catalog?genre=Adventure");?>">Adventure</a></li>
                                                    <li><a href="<?= site_url("/catalog?genre=Puzzle");?>">Puzzle</a></li>
                                                    <li><a href="<?= site_url("/catalog?genre=Racing");?>">Racing</a></li>
                                                    <li><a href="<?= site_url("/catalog?genre=Sport");?>">Sport</a></li>
                                                <?php }
                                            ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--<div class="card">
                                    <div class="card-heading">
                                        <a data-toggle="collapse" data-target="#collapseSix">Tag</a>
                                    </div>
                                    <div id="collapseSix" class="collapse show" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="shop__sidebar__tags">
                                                <a href="#">Playstation 1</a>
                                                <a href="#">Playstation 2</a>
                                                <a href="#">Playstation 3</a>
                                                <a href="#">Playstation 4</a>
                                                <a href="#">Playstation 5</a>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="shop__product__option">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="shop__product__option__left">
                                    <p><?php 
                                        $akhir = $page * 12;
                                        $awal = $akhir - 12 ;
                                        $awal = ($akhir > 12) ? $awal : 1;
                                        $akhir = ($jumlah > 12) ? 12 : $jumlah;
                                        $hasil = ($jumlah <= 12) ? '1-'.$jumlah : $awal."-".$akhir;
                                        echo $hasil;
                                    ?> dari <?=$jumlah?> hasil</p>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="shop__product__option__right">
                                    <p>Sort by Price:</p>
                                    <select>
                                        <option value="">Low To High</option>
                                        <option value="">High To Low</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <?php $counter = 0;
                        foreach($barang->getResultArray() as $row): ?>
                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <div class="product__item">
                                    <div class="product__item__pic set-bg" data-setbg="img/product/default.png">
                                        <ul class="product__hover">
                                            <li><a href="#"><img src="img/icon/Love.svg" style="background: none;" width="40px" height="40px" alt=""><span>Whistlist</span></a></li>
                                            <li><a href="#"><img src="img/icon/Search.svg" style="background: none;" width="40px" height="40px" alt=""><span>Detail</span></a></li>
                                        </ul>
                                    </div>
                                    <div class="product__item__text">
                                        <h6><?=$row['namaBarang']?></h6>
                                        <a href="#" class="add-cart" id='<?=$row['idBarang']?>'>+ Add To Cart</a>
                                        <div class="rating">
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        </div>
                                        <h5>Rp <?=number_format($row['hargaBarang'],0,",",".")?></h5>
                                    </div>
                                </div>
                            </div>
                        <?php
                            $counter++;
                        endforeach;?>
                        <?php
                            while($counter % 3 != 0){ ?>
                                <div class="col-lg-4 col-md-6 col-sm-6">
                                </div> <?php
                                $counter++;
                            }
                        ?>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="product__pagination">
                                <!-- LINK NUMBER -->
                                <?php
                                // Buat query untuk menghitung semua jumlah data
                                $jumlah_page = ceil($jumlah / 12); // Hitung jumlah halamanya
                                $jumlah_number = 3; // Tentukan jumlah link number sebelum dan sesudah page yang aktif
                                $start_number = ($page > $jumlah_number) ? $page - $jumlah_number : 1; // Untuk awal link member
                                $end_number = ($page < ($jumlah_page - $jumlah_number)) ? $page + $jumlah_number : $jumlah_page; // Untuk akhir link number
                                for ($i = $start_number; $i <= $end_number; $i++) {
                                    $link_active = ($page == $i) ? 'class="active"' : '';
                                    if(isset($_GET['cari']) || isset($_GET['minVal']) || isset($_GET['genre'])){
                                ?>
                                    <a href=<?php echo site_url("$_SERVER[REQUEST_URI]"."&page=".$i); ?> <?=$link_active?>><?php echo $i; ?></a>
                                <?php
                                    }else{
                                ?>
                                    <a href=<?php echo site_url("$_SERVER[REQUEST_URI]"."?page=".$i); ?> <?=$link_active?>><?php echo $i; ?></a>
                                <?php
                                    }
                                }
                                ?>
                                <!--<a class="active" href="#">1</a>
                                <a href="#">2</a>
                                <a href="#">3</a>
                                <span>...</span>
                                <a href="#">21</a>-->
                            </div>
                        </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                    </div>
                    <div id="isi">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Shop Section End -->

    

    <!-- Search Begin -->
    <div class="search-model">
        <div class="h-100 d-flex align-items-center justify-content-center">
            <div class="search-close-switch">+</div>
            <form class="search-model-form">
                <input type="text" id="search-input" placeholder="Search here.....">
            </form>
        </div>
    </div>
    <!-- Search End -->

    <!-- Js Plugins -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery.nicescroll.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/mixitup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function (){
            $(".add-cart").on('click', function(event){
                //console.log(this.id);
                var idBarang = this.id;
                $.ajax({
                    type: "POST",
                    url: "<?=site_url('public/tombolCart')?>",
                    data: { barang :  idBarang , jumlah : 1},
                    success: function(data){
                        alert(JSON.stringify(data));
                        //$("input[name='csrf_test_name']").val(result['csrf']);
                    },
                    error: function(response) {
                        alert(response.status);
                    }
                });
            });
        });
    </script>
</body>

</html>