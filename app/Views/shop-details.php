<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Male_Fashion Template">
    <meta name="keywords" content="Male_Fashion, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PlayStation Corner | Detail</title>

    <!-- Shop Details Section Begin -->
    <section class="shop-details">
        <div class="product__details__pic">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="product__details__breadcrumb">
                            <a href="./index.html">Home</a>
                            <a href="./shop.html">Shop</a>
                            <span>Product Details</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <ul class="nav nav-tabs" role="tablist">
                            
                        </ul>
                    </div>
                    <div class="col-lg-6 col-md-9">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tabs-1" role="tabpanel">
                                <div class="product__details__pic__item">
                                    <!-- Disni Ganti Gambar -->
                                    <img src="img/product/<?=$pilihan->idBarang?>.jpg" alt="">
                                    <!-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->
                                </div>
                            </div>
                            <div class="tab-pane" id="tabs-2" role="tabpanel">
                                <div class="product__details__pic__item">
                                    <img src="img/shop-details/product-big-3.png" alt="">
                                </div>
                            </div>
                            <div class="tab-pane" id="tabs-3" role="tabpanel">
                                <div class="product__details__pic__item">
                                    <img src="img/shop-details/product-big.png" alt="">
                                </div>
                            </div>
                            <div class="tab-pane" id="tabs-4" role="tabpanel">
                                <div class="product__details__pic__item">
                                    <img src="img/shop-details/product-big-4.png" alt="">
                                    <a href="https://www.youtube.com/watch?v=8PJ3_p7VqHw&list=RD8PJ3_p7VqHw&start_radio=1" class="video-popup"><i class="fa fa-play"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="product__details__content">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-lg-8">
                        <div class="product__details__text">
                            <h4><?=$pilihan->namaBarang?></h4>
                            <div class="rating">
                                <?php
                                    $ctr = 0;
                                    for($i = 0; $i < 5; $i++)
                                    {
                                        if($ctr < $bintang) echo "<i class='fa fa-star'></i>";
                                        else echo"<i class='fa fa-star-o'></i>";
                                        $ctr++;
                                    }
                                ?>
                                <!-- <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i> -->
                                <span> - <?=$jumlahReview?> Reviews</span>
                            </div>
                            <h3>Rp. <?=$pilihan->hargaBarang?></h3>
                            <p><?=$pilihan->deskripsiBarang?></p>
                            <div class="product__details__cart__option">
                                <div class="quantity">
                                    <div class="pro-qty">
                                        <input type="text" value="1" id='qty-barang'>
                                    </div>
                                </div>
                                <a href="#" class="primary-btn" id='beli'>Tambahkan Keranjang</a>
                            </div>
                            <div class="product__details__last__option">
                                
                                <ul>
                                    <?php 
                                        if($pilihan->kategoriBarang == 'Games'){
                                    ?>
                                    <li><span>ID</span> 3812912</li>
                                    <li><span>Categories: </span><?=$pilihan->kategoriBarang?></li>
                                    <li><span>Tag:</span> <?=$pilihan->genre?></li>
                                        <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="product__details__tab">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tabs-6" role="tab">Customer
                                    Testimoni(<?=$jumlahReview?>)</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane" id="tabs-6" role="tabpanel">
                                    <div class="product__details__tab__content">
                                        <div class="product__details__tab__content__item">
                                            <h4>Testimoni Dari Pembeli</h4>
                                            <?php
                                                foreach($testimoni as $row){
                                                    echo "<h5>".$row->emailUser."</h5>";
                                                    echo "<h5>".$row->reviewTestimoni." ($row->bintangRating/5)"."</h5>";
                                                }
                                            ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Shop Details Section End -->

    <!-- Related Section Begin -->
    <section class="related spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="related-title">Produk Terkait</h3>
                </div>
            </div>
            <div class="row">
                <?php
                    foreach($rekomendasi as $row){
                ?>
                <div class="col-lg-3 col-md-6 col-sm-6 col-sm-6">
                    <div class="product__item">
                        <div class="product__item__pic set-bg" data-setbg="img/product/<?=$row->idBarang?>.jpg">
                            <ul class="product__hover">
                                <li><a href="<?=site_url("public/detail?id=").$row->idBarang?>"><img src="img/icon/search.png" alt=""></a></li>
                            </ul>
                        </div>
                        <div class="product__item__text">
                            <h6><?=$row->namaBarang?></h6>
                            <a href="#" class="add-cart" id='<?=$row->idBarang?>'>+ Add To Cart</a>
                            <div class="rating">
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                            </div>
                            <h5>Rp. <?=$row->hargaBarang?></h5>
                        </div>
                    </div>
                </div>
                    <?php }?>
            </div>
        </div>
    </section>
    <!-- Related Section End -->

    

    <!-- Js Plugins -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery.nicescroll.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/mixitup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>
    <script type="text/javascript">
        $(document).ready(function (){
            var getUrlParameter = function getUrlParameter(sParam) {
                var sPageURL = window.location.search.substring(1),
                    sURLVariables = sPageURL.split('&'),
                    sParameterName,
                    i;

                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                    }
                }
            };
            $(".add-cart").on('click', function(event){
                //console.log(this.id);
                var idBarang = this.id;
                $.ajax({
                    type: "POST",
                    url: "<?=site_url('public/tombolCart')?>",
                    dataType: 'json',
                    data: { barang :  idBarang , jumlah : 1},
                    success: function(data){
                        alert(data.pesan);
                        //$("input[name='csrf_test_name']").val(result['csrf']);
                    },
                    error: function(response) {
                        alert(response.status);
                    }
                });
            });
            $("#beli").on('click', function(event){
                var idBarang = getUrlParameter('id');
                var qty = $('#qty-barang').val();
                $.ajax({
                    type: "POST",
                    url: "<?=site_url('public/tombolCart')?>",
                    dataType: 'json',
                    data: { barang :  idBarang , jumlah : qty},
                    success: function(data){
                        alert(data.pesan);
                    },
                    error: function(response) {
                        alert(response.status);
                    }
                });
            });
        });
        
    </script>
</body>

</html>