<?php namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table = 'user';
    protected $primaryKey = 'emailUser';
    protected $allowedFields = ['emailUser','namaUser','passwordUser','statusUser'];

    public function __construct(){
        parent ::__construct();
        $this->db = db_connect();
    }
    function fetch_data()
    {
        return $this->db->table('user')->get();
    }
    function getData($email,$password)
    {
        $builder = $this->db->table('user');
        $builder->select('*');
        $arr = array('emailUser' => $email, 'passwordUser' => $password);
        $builder->where($arr);
        return $builder->get();
        //$array = array('name !=' => $name, 'id <' => $id, 'date >' => $date);
    }
    function getDatabyEmail($email)
    {
        $builder = $this->db->table('user');
        $builder->select('*');
        $arr = array('emailUser' => $email);
        $builder->where($arr);
        return $builder->get();
    }
    function getAlamatbyEmail($email)
    {
        $builder = $this->db->table('alamatuser');
        $builder->select('*');
        $arr = array('emailUser' => $email);
        $builder->where($arr);
        return $builder->get();
    }
    function insertData($email,$nama,$password,$status)
    {
        $userBaru = new UserModel();
        $data = [
            'emailUser' => $email,
            'namaUser' => $nama,
            'passwordUser' => $password,
            'statusUser' => $status
        ];
        $hasil = $userBaru->insert($data);
    }
    function updateData($email,$nama)
    {
        $userModel = new UserModel();
        $data = [
            'emailUser' => $email,
            'namaUser' => $nama
        ];
        return $userModel->update($email,$data);
    }
    function updatePass($email,$newpass)
    {
        $userModel = new UserModel();
        $data = [
            'emailUser' => $email,
            'passwordUser' => $newpass
        ];
        return $userModel->update($email,$data);
    }
    function getAll()
    {
        $builder = $this->db->table('user');
        $builder->select('*');
        return $builder->get();
    }
    function getNama($email)
    {
        $builder = $this->db->table('user');
        $builder->select('namaUser');
        $builder->where('emailUser',$email);
        return $builder->get();
    }
}