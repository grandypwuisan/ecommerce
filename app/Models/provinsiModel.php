<?php namespace App\Models;

use CodeIgniter\Model;

class provinsiModel extends Model
{
    public function getProvinsi(){
        return $this->db->table('provinsi')->get();
    }

    public function getKotabyProvinsi($idProv){
        $builder = $this->db->table('kabupaten');
        $builder->select('*');
        $arr = array('id_prov' => $idProv);
        $builder->where($arr);
        return $builder->get();
    }
    public function getKecamatanByKota($idKab){
        $builder = $this->db->table('kecamatan');
        $builder->select('*');
        $arr = array('id_kab' => $idKab);
        $builder->where($arr);
        return $builder->get();
    }
    public function getKeluarahanbyKecamatan($idKec){
        $builder = $this->db->table('kabupaten');
        $builder->select('*');
        $arr = array('id_prov' => $idKec);
        $builder->where($arr);
        return $builder->get();
    }
}