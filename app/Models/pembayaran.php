<?php namespace App\Models;

use CodeIgniter\Model;

class Pembayaran extends Model
{
    protected $table = 'pembayaran';
    protected $primaryKey = 'idTransaksi';
    protected $allowedFields = ['idTransaksi','idTrans','status'];

    public function __construct(){
        parent ::__construct();
        $this->db = db_connect();
    }
    function getData($id)
    {
        $builder = $this->db->table('pembayaran');
        $builder->select('*');
        $builder->where('idTrans',$id);
        return $builder->get();
    }
}