<?php namespace App\Models;

use CodeIgniter\Model;

class Cart extends Model
{
    protected $table = 'cart';
    protected $primaryKey = 'idCart';
    protected $allowedFields = ['emailUser','idBarang','kuantitas'];

    public function __construct(){
        parent ::__construct();
        $this->db = db_connect();
    }
    function insertData($email,$idBarang,$kuantitas)
    {
        $cartBaru = new Cart();
        $data = [
            'emailUser' => $email,
            'idBarang' => $idBarang,
            'kuantitas' => $kuantitas
        ];
        $hasil = $cartBaru->insert($data);
    }
    function getUserCart($emailUser)
    {
        $builder = $this->db->table('cart');
        $builder->select('*');
        $builder->where('emailUser',$emailUser);
        return $builder->get();
    }
    function getCart($email)
    {
        $builder = $this->db->table('tampilanCart');
        $builder->select('*');
        $builder->where('email',$email);
        return $builder->get();
    }
    function checkCart($email,$idBarang)
    {
        $builder = $this->db->table('cart');
        $builder->select('*');
        $builder->where(['emailUser' => $email,'idBarang' => $idBarang]);
        return $builder->countAllResults();
    }
    function updateQty($email,$idBarang,$qty)
    {
        $builder = $this->db->table('cart');
        $builder->set('kuantitas', "kuantitas+$qty", FALSE);
        $builder->where(['emailUser' => $email, 'idBarang' => $idBarang]);
        $builder->update();
    }
    function getOneCart($email,$idBarang)
    {
        $builder = $this->db->table('cart');
        $builder->select('*');
        $builder->where(['emailUser' => $email,'idBarang' => $idBarang]);
        return $builder->get();
    }
    function deleteCart($email,$idBarang)
    {
        $builder = $this->db->table('cart');
        $builder->where(['emailUser' => $email,'idBarang' => $idBarang]);
        $builder->delete();
        return "terhapus";
    }
    function cekHargaCart($email,$idBarang)
    {
        $builder = $this->db->table('tampilanCart');
        $builder->select('*');
        $builder->where(['email' => $email,'id' => $idBarang]);
        return $builder->get();
    }
    function updateCart($email,$idBarang,$jumlahBaru)
    {
        $builder = $this->db->table('cart');
        $builder->set('kuantitas', $jumlahBaru);
        $builder->where(['emailUser' => $email, 'idBarang' => $idBarang]);
        $builder->update();
    }
    function deleteCartUser($email)
    {
        $builder = $this->db->table('cart');
        $builder->where('emailUser', $email);
        $builder->delete();
    }
}