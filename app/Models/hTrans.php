<?php namespace App\Models;

use CodeIgniter\Model;

class HTrans extends Model
{
    protected $table = 'htrans';
    protected $primaryKey = 'idTrans';
    protected $allowedFields = ['idTrans','emailUser','tglTransaksi','tglPemesanan','statusPembayaran','totalPembayaran','noResi','ongkir'
    ,'alamatPengiriman','vakun'];

    public function __construct(){
        parent ::__construct();
        $this->db = db_connect();
    }
    function getData($id)
    {
        $builder = $this->db->table('htrans');
        $builder->select('*');
        $builder->where('idTrans',$id);
        return $builder->get();
    }
    function getDataByEmail($email)
    {
        $builder = $this->db->table('htrans');
        $builder->select('*');
        $builder->where('emailUser',$email);
        return $builder->get();
    }
    function updateTglTransaksi($tanggal,$id){
        $builder = $this->db->table('htrans');
        $builder->set('tglTransaksi',$tanggal);
        $builder->where('idTrans',$id);
        $builder->update();
    }
    function countData(){
        $builder = $this->db->table('htrans');
        $builder->select("*");
        return $builder->countAllResults();
    }
    function getBelum()
    {
        $builder = $this->db->table('adminpesanan');
        $builder->select("*");
        return $builder->get();
    }
    function updateResi($id,$resi)
    {
        $builder = $this->db->table('htrans');
        $builder->set('statusPembayaran', 'Terkirim');
        $builder->set('noResi', $resi);
        $builder->where('idTrans',$id);
        $builder->update();
    }
    function updateStatus($id,$status){
        $builder = $this->db->table('htrans');
        $builder->set('statusPembayaran',$status);
        $builder->where('idTrans',$id);
        $builder->update();
    }
    function cekStatus($id){
        $builder = $this->db->table('htrans');
        $builder->select('statusPembayaran');
        $builder->where('idTrans',$id);
        return $builder->get();
    }
}