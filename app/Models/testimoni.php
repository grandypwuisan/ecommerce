<?php namespace App\Models;

use CodeIgniter\Model;

class Testimoni extends Model
{
    protected $table = 'testimoni';
    protected $primaryKey = 'idTestimoni';
    protected $allowedFields = ['emailUser','idBarang','idTestimoni','reviewTestimoni','bintangRating'];
    public function __construct(){
        parent ::__construct();
        $this->db = db_connect();
    }

    function getReview($idBarang)
    {
        $builder = $this->db->table('testimoni');
        $builder->where('idBarang', $idBarang);
        return $builder->get();
    }
    function getCount($idBarang)
    {
        $builder = $this->db->table('testimoni');
        $builder->where('idBarang', $idBarang);
        return $builder->countAllResults();
    }
    
}