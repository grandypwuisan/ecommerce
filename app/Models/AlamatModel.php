<?php namespace App\Models;

use CodeIgniter\Model;

//Percobaan pertama
class AlamatModel extends Model
{
    protected $table = 'alamatuser';
    protected $primaryKey = 'idAlamat';
    protected $allowedFields = ['emailUser','idAlamat','namaJalan','kodePos','provinsi','kota'];

    public function __construct(){
        parent ::__construct();
        $this->db = db_connect();
    }
    function fetch_data()
    {
        return $this->db->table('alamatuser')->get();
    }
    function getData($email)
    {
        $builder = $this->db->table('alamatuser');
        $builder->select('*');
        $builder->where('emailUser',$email);
        return $builder->get();
        //$array = array('name !=' => $name, 'id <' => $id, 'date >' => $date);
    }
    function insertData($email,$id,$namaJalan,$kodePos,$provinsi,$kota,$idKota,$kec,$telp)
    {
        $builder = $this->db->table('alamatuser');
        $data = [
            'emailUser' => $email,
            'idAlamat' => $id,
            'namaJalan' => $namaJalan,
            'kodePos' => $kodePos,
            'provinsi' => $provinsi,
            'kota' => $kota,
            'id_kota'=>$idKota,
            'kecamatan'=>$kec,
            'telpon'=>$telp
        ];
        $builder->insert($data);
        return $builder->get();
    }
    function getAll()
    {
        $builder = $this->db->table('alamatuser');
        $builder->select('*');
        return $builder->get();
    }
    function updateData($email,$namaJalan,$kodePos,$provinsi,$kota,$idKota,$kec,$telp)
    {
        $builder = $this->db->table('alamatuser');
        $data = [
            'namaJalan' => $namaJalan,
            'kodePos' => $kodePos,
            'provinsi' => $provinsi,
            'kota' => $kota,
            'id_kota'=>$idKota,
            'kecamatan'=>$kec,
            'telpon'=>$telp
        ];
        $builder->where('emailUser', $email);
        $builder->update($data);
        return $builder->get();
    }
    function getId($email)
    {
        $builder = $this->db->table('alamatuser');
        $builder->select('idAlamat');
        $builder->where('emailUser',$email);
        return $builder->get();
    }
}