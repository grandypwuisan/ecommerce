<?php namespace App\Models;

use CodeIgniter\Model;

class Promo extends Model
{
    protected $table = 'promo';
    protected $allowedFields = ['namaPromo','diskon','deskripsiPromo','kodePromo'];

    public function __construct(){
        parent ::__construct();
        $this->db = db_connect();
    }
    function getPromo($kode)
    {
        $builder = $this->db->table('promo');
        $builder->select('*');
        $builder->where('kodePromo',$kode);
        return $builder->get();
    }
}