<?php namespace App\Models;

use CodeIgniter\Model;
use CodeIgniter\I18n\Time;
class Barang extends Model
{
    protected $table = 'barang';
    protected $primaryKey = 'idBarang';
    protected $allowedFields = ['namaBarang','hargaBarang','idBarang','deskripsiBarang','stokBarang','statusBarang'
    ,'kategoriBarang','tanggalRilis','genre'];

    public function __construct(){
        parent ::__construct();
        $this->db = db_connect();
    }
    function fetch_data($data)
    {
        $limit_start = ($data['page'] - 1) * 12;
        $builder = $this->db->table('barang');
        $builder->select('*');
        if($data['cari'] != ""){
            $builder->like('namaBarang',$data['cari']);
        }
        if($data['kategori'] != ""){
            $builder->like('kategoriBarang',$data['kategori']);
        }
        if($data['genre'] != ""){
            $builder->like('genre', $data['genre']);
        }
        if($data['minVal'] != ""){
            $builder->where('hargaBarang >=', $data['minVal']);
        }
        if($data['maxVal'] != ""){
            $builder->where('hargaBarang <=', $data['maxVal']);
        }
        if($data['status'] != ""){
            if($data['status'] == "Pre-Order"){
                $builder->where('statusBarang', $data['status']);
            }else if($data['status'] == "NewRelease"){
                //$builder->where('statusBarang', 'Ready');
                $time = time();
                $builder->where('tanggalRilis >' ,date('Y-m-d', strtotime('-1 month', $time)));
                $builder->where('tanggalRilis <', date("Y-m-d",$time));
            }
        }
        if($data['sort'] != ""){
            $builder->orderBy('hargaBarang',$data['sort']);
        }
        $builder->limit(12,$limit_start);
        return $builder->get();
    }
    function getData($id)
    {
        $builder = $this->db->table('barang');
        $builder->select('*');
        $builder->where('idBarang',$id);
        return $builder->get();
    }
    function getBarang($id)
    {
        $builder = $this->db->table('barang');
        $builder->select('*');
        $builder->where('idBarang',$id);
        return $builder->get();
    }
    function getJumlahData($data){
        $builder = $this->db->table('barang');
        $builder->select("*");
        if($data['cari'] != ""){
            $builder->like('namaBarang',$data['cari']);
        }
        if($data['kategori'] != ""){
            $builder->like('kategoriBarang',$data['kategori']);
        }
        if($data['genre'] != ""){
            $builder->like('genre', $data['genre']);
        }
        if($data['minVal'] != ""){
            $builder->where('hargaBarang >=', $data['minVal']);
        }
        if($data['maxVal'] != ""){
            $builder->where('hargaBarang <=', $data['maxVal']);
        }
        if($data['status'] != ""){
            if($data['status'] == "Pre-Order"){
                $builder->where('statusBarang', $data['status']);
            }else if($data['status'] == "NewRelease"){
                //$builder->where('statusBarang', 'Ready');
                //$builder->where('tanggalRilis >' ,'2020-10-09');
                //$builder->where('tanggalRilis <', '2020-12-09');
                $time = time();
                $builder->where('tanggalRilis >' ,date('Y-m-d', strtotime('-1 month', $time)));
                $builder->where('tanggalRilis <', date("Y-m-d",$time));
            }
        }
        return $builder->countAllResults();
    }
    function getKategori(){
        $builder = $this->db->table('barang');
        $builder->distinct();
        $builder->select('kategoriBarang');
        return $builder->get();
    }
    function getGenre(){
        $builder = $this->db->table('barang');
        $builder->distinct();
        $builder->select('genre');
        return $builder->get();
    }
    function getStatus(){
        $builder = $this->db->table('barang');
        $builder->distinct();
        $builder->select('statusBarang');
        return $builder->get();
    }
    function insertData($namaBarang,$hargaBarang,$idBarang,$deskripsi,$stokBarang,$status,$kategori,$tglrilis,$genre,$berat)
    {
        $barangBaru = new Barang();
        $data = [
            'namaBarang' => $namaBarang,
            'hargaBarang' => $hargaBarang,
            'idBarang' => $idBarang,
            'deskripsiBarang' => $deskripsi,
            'stokBarang' => $stokBarang,
            'statusBarang'=>$status,
            'kategoriBarang'=>$kategori,
            'tanggalRilis' => $tglrilis,
            'genre' => $genre,
            'beratBarang' => $berat
        ];
        $hasil = $barangBaru->insert($data);
    }
    function getCode($tipe)
    {
        $builder = $this->db->table('barang');
        $builder->select('*');
        $builder->like('idBarang', $tipe);
        return $builder->get();
    }
    function updateData($idBarang,$namaBarang,$hargaBarang,$deskripsi,$stokBarang,$status,$kategori,$genre,$berat)
    {
        $builder = $this->db->table('barang');
        $data = [
            'namaBarang' => $namaBarang,
            'hargaBarang'  => $hargaBarang,
            'deskripsiBarang'  => $deskripsi,
            'stokBarang' => $stokBarang,
            'statusBarang' => $status,
            'kategoriBarang' => $kategori,
            'genre' => $genre,
            //'tanggalRilis' => $tglrilis,
            'beratBarang'=>$berat
        ];
        $builder->where('idBarang', $idBarang);
        $builder->update($data);
    }
    function getRecomendation($kategori,$jumlah)
    {
        $builder = $this->db->table('barang');
        $builder->select('*');
        $builder->where('kategoriBarang', $kategori);
        $builder->orderBy('idBarang', 'Random');
        $builder->limit($jumlah);
        return $builder->get();
    }
    function getByCategory($kategori)
    {
        $builder = $this->db->table('barang');
        $builder->select('*');
        $builder->like('kategoriBarang', $kategori);
        return $builder->get();
    }
    function cariMaster($nama,$kategori,$bawah,$atas)
    {
        $builder = $this->db->table('barang');
        $builder->select('*');
        $builder->like('kategoriBarang', $kategori);
        $builder->like('namaBarang', $nama);
        $builder->where('hargaBarang >=', $bawah);
        $builder->where('hargaBarang <', $atas);
        return $builder->get();
    }
    function updateStok($stok,$id)
    {
        $builder = $this->db->table('barang');
        $builder->set('stokBarang',$stok);
        $builder->where('idBarang',$id);
        $builder->update();
    }
}