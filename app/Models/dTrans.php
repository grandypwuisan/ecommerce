<?php namespace App\Models;

use CodeIgniter\Model;

class DTrans extends Model
{
    protected $table = 'dtrans';
    protected $allowedFields = ['idBarang','idTrans','kuantitas','subTotal','hargaBarang'];

    public function __construct(){
        parent ::__construct();
        $this->db = db_connect();
    }
    function getData($id)
    {
        $builder = $this->db->table('dtrans');
        $builder->select('*');
        $builder->where('idTrans',$id);
        return $builder->get();
    }
    function getDataDetail($id){
        $builder = $this->db->table('dtrans');
        $builder->select('*');
        $builder->where('idTrans',$id);
        $builder->join('barang', 'barang.idBarang = dtrans.idBarang');
        return $builder->get();
    }
}