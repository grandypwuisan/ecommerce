<?php namespace Config;

class Validation
{
	//--------------------------------------------------------------------
	// Setup
	//--------------------------------------------------------------------

	/**
	 * Stores the classes that contain the
	 * rules that are available.
	 *
	 * @var array
	 */
	public $ruleSets = [
		\CodeIgniter\Validation\Rules::class,
		\CodeIgniter\Validation\FormatRules::class,
		\CodeIgniter\Validation\FileRules::class,
		\CodeIgniter\Validation\CreditCardRules::class,
	];

	/**
	 * Specifies the views that are used to display the
	 * errors.
	 *
	 * @var array
	 */
	public $templates = [
		'list'   => 'CodeIgniter\Validation\Views\list',
		'single' => 'CodeIgniter\Validation\Views\single',
	];

	//--------------------------------------------------------------------
	// Rules
	//--------------------------------------------------------------------
	public $register = [
		'email' => 'required|valid_email|is_unique[user.emailUser]',
		'pass' => 'required|min_length[8]|max_length[20]',
		'cpass' => 'required|matches[pass]'
	];
	 
	public $register_errors = [
		'email' => [
			'required'          => 'Email wajib diisi',
			'email.valid_email' => 'Email tidak valid',
			'is_unique' => 'Email yang anda masukan telah digunakan'
		],
		'pass' => [
			'required'      => 'Password wajib diisi',
			'min_length'    => 'Password minimal terdiri dari 8 karakter',
			'max_length'    => 'Password maksimal terdiri dari 20 karakter'
		],
		'cpass' => [
			'matches' => 'Confirm Password tidak sesuai dengan password'
		]
	];


	public $login = [
		'email' => 'required',
		'pass' => 'required'
	];

	public $login_errors = [
		'email' => [
			'required'          => 'Email wajib diisi'
		],
		'pass' => [
			'required'      => 'Password wajib diisi'
		]
	];

	public $editAlamat = [
		'namaJalan' => 'required',
		'kodePos'=> 'required',
		'kecamatan'=> 'required',
		'kota'=> 'required',
		'telpon'=> 'required|min_length[8]|max_length[20]'
	];

	public $editAlamat_errors = [
		'namaJalan' => [
			'required'          => 'Alamat wajib diisi'
		],
		'kodePos' => [
			'required'          => 'Kode Pos wajib diisi'
		],
		'kecamatan' => [
			'required'          => 'Kecamatan wajib diisi'
		],
		'kota' => [
			'required'          => 'Kota wajib diisi'
		],
		'telpon' => [
			'required'          => 'Nomor Telpon wajib diisi',
			'min_length'    => 'Nomor Telpon minimal terdiri dari 9 karakter',
			'max_length'    => 'Nomor Telpon maksimal terdiri dari 12 karakter'
		]
	];
}
