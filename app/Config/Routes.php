<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('psc');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/home', 'psc::index');
$routes->get('/catalog', 'psc::catalog');
$routes->get('/about', 'psc::about');
$routes->get('/faq', 'psc::faq');
$routes->get('/login', 'psc::login');
$routes->get('/profile', 'psc::profile');
$routes->get('/masterinsert', 'psc::masterinsert');
$routes->get('/alamat', 'psc::alamat');
$routes->get('/getCity', 'psc::getCity');
$routes->get('/logout', 'psc::logout');

$routes->post('/prosesLogin', 'psc::prosesLogin');
$routes->post('/prosesRegister', 'psc::prosesRegister');
$routes->post('/insertBarang','psc::adminBarang');
$routes->post('/tombolCart','psc::tambahCart');
$routes->post('/editprofile', 'psc::editprofile');
$routes->post('/editalamat', 'psc::editAlamat');

$routes->get('/catalog', 'psc::catalog');
$routes->get('/snap','psc::snap');
$routes->post('/finish','psc::finish');
$routes->get('/token','psc::token');
$routes->post('/loadBarang','psc::loadBarang');
$routes->get('/tesCart','psc::tescart');

$routes->get('/editBarang','psc::editBarang');

$routes->post('/updateBarang','psc::updtBarang');

$routes->get('/ajax','psc::tes');

$routes->get('/detail','psc::toDetail');
$routes->post('/cariBarang','psc::cariBarang');
$routes->get('/toCart','psc::toCart');
$routes->post('/hapusCart','psc::hapusCart');

$routes->get('/history','psc::history');
$routes->get('/orderdetails','psc::orderDetails');
$routes->post('/pakaiPromo','psc::pakaiPromo');
$routes->post('/ubahCart','psc::ubahCart');
$routes->get('/adminPesanan','psc::adminPesanan');
$routes->post('/updateResi','psc::updateResi');
$routes->get('/detailAdminPesanan','psc::detailAdminPesanan');
$routes->get('/qwerty','psc::updateResi');
/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
