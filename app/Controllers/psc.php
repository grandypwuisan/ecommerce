<?php namespace App\Controllers;

use App\Models\AlamatModel;
use CodeIgniter\Controller;
use App\Models\userModel;
use App\Models\Barang;
use App\Models\cart;
use App\Models\provinsiModel;
use App\Models\HTrans;
use App\Models\DTrans;
use App\Models\promo;
use App\Models\Pembayaran;
use App\Libraries\Midtrans;
use CodeIgniter\CLI\Console;
use CodeIgniter\Exceptions\AlertError;
use App\Models\testimoni;

class Psc extends BaseController
{
	private $api_key = "dc9a4af03e9dba0af7c2f3fd20177e84";
	private $url = "https://api.rajaongkir.com/starter/";
	public function __construct()
    {
        helper(['form','cookie','session','security']);
		$this->form_validation = \Config\Services::validation();
		$session=session();
		//library('midtrans');
		include(APPPATH.'libraries\Midtrans.php');
		$midtrans = new Midtrans();
		$params = array('server_key' => 'SB-Mid-server-GRZAMLuRKm6e0Ft4syZeIuuX', 'production' => false);
		$midtrans->config($params);
		helper('url');
    }
	public function index()
	{
		echo view('header');
		echo view('home');
		echo view('footer');
	}
	public function catalog()
	{
		helper('number');
		echo view('header');
		$data_b = new Barang();
		$data_kirim = [
			'page' => (isset($_GET['page'])) ? $_GET['page'] : 1,
			'cari' => (isset($_GET['cari'])) ? $_GET['cari'] : "",
			'kategori' => (isset($_GET['kategori'])) ? $_GET['kategori'] : "",
			'minVal' => (isset($_GET['minVal'])) ? $_GET['minVal']: "",
			'maxVal' =>(isset($_GET['maxVal'])) ? $_GET['maxVal']: "",
			'genre' => (isset($_GET['genre'])) ? $_GET['genre']: "",
			'status' => (isset($_GET['status'])) ? $_GET['status']: '',
			'sort' => (isset($_GET['sort'])) ? $_GET['sort']: ''
		];
		$data = [
			'barang'=> $data_b->fetch_data($data_kirim),
			'page'=>$data_kirim['page'],
			'jumlah'=> $data_b->getJumlahData($data_kirim),
			'kategori'=>$data_b->getKategori(),
			'genre'=>$data_b->getGenre(),
			'status'=> (string)$data_kirim['status'],
			'sort'=> $data_kirim['sort']
		];
		echo view('shop1.php',$data);
		echo view('footer');
	}
	public function loadBarang()
	{
		helper('number');
		$data_b = new Barang();
		$data_kirim = [
			'page' => (isset($_POST['page'])) ? $_POST['page'] : 1,
			'cari' => (isset($_POST['cari'])) ? $_POST['cari'] : "",
			'kategori' => (isset($_POST['kategori'])) ? $_POST['kategori'] : "",
			'minVal' => (isset($_POST['minVal'])) ? $_POST['minVal']: "",
			'maxVal' =>(isset($_POST['maxVal'])) ? $_POST['maxVal']: "",
			'genre' => (isset($_POST['genre'])) ? $_POST['genre']: "",
			'status' => (isset($_POST['status']))? $_POST['status']: "",
			'sort' => (isset($_POST['sort'])) ? $_POST['sort']: ''
		];
		$data = [
			'barang'=> $data_b->fetch_data($data_kirim),
			'page'=>$data_kirim['page'],
			'jumlah'=> $data_b->getJumlahData($data_kirim),
			'kategori'=>$data_b->getKategori(),
			'genre'=>$data_b->getGenre()
		];
		echo view('showBarang',$data);
	}
	public function about()
	{
		echo view('header');
		echo view('about');
		echo view('footer.php');
	}
	public function faq()
	{
		echo view('header');
		echo view('faq');
		echo view('footer.php');
	}
	public function login()
	{
		echo view('header');
		echo view('login');
		echo view('footer.php');
	}
	public function masterinsert()
	{
		if(session('email_login') == 'admin')
		{
			echo view('loadCssJs');
			echo view('headerAdmin');
			echo view('masterinsert');
		}
		else return redirect()->to(site_url('public/login'));
	}
	public function prosesRegister()
	{
		$validation =  \Config\Services::validation();
		$db = \Config\Database::connect();
		
		$userBaru['email'] = $_POST['email'];
		$userBaru['pass'] = $_POST['pass'];
		$userBaru['cpass'] = $_POST['cpass'];
		if($this->form_validation->run($userBaru, 'register') == FALSE)
		{
            // mengembalikan nilai input yang sudah dimasukan sebelumnya
            //session()->setFlashdata('inputs', $this->request->getPost());
            // memberikan pesan error pada saat input data
            session()->setFlashdata('errorReg', $this->form_validation->getErrors());
            // kembali ke halaman form
			return redirect()->to(site_url('public/login'));
			//print_r($this->form_validation->getErrors());
		} 
		else 
		{
			session()->setFlashdata('successReg', 'Registrasi Berhasil');
            // kembali ke halaman register
			//return redirect()->to(base_url('home/register'));
			$user = new userModel();
			$user -> insertData($_POST['email'],'User',md5($_POST['cpass']),'Baru');
			return redirect()->to(site_url('public/login'));
        }
	}
	public function prosesLogin()
	{
		$emailUser = $_POST['email'];
		$email = $_POST['email'];
		$pass1 = $_POST['pass'];
		$pass = md5($_POST['pass']);
		//$remember = $_POST['remember-me'];
		$hasil = new userModel();
		if($email == 'admin' && $pass1 == 'admin')
		{
			session()->set('email_login', 'admin');
			return redirect()->to(site_url('public/masterinsert'));
		}
		else 
		{
			if(count($hasil -> getData($email,$pass) -> getResultArray()) == 1)
			{
				session()->setFlashdata('successLog', 'login Berhasil');
				session()->set('email_login', $emailUser);
				//kalau ambil ->get('nama_variabel')
				return redirect()->to(site_url('public/home'));
				//isset($_SESSION['some_name']) <- cek sdh ad nd session
			}
			else
			{
				session()->setFlashdata('errorLog', 'login gagal');
				// kembali ke halaman form
				return redirect()->to(site_url('public/login'));
			}
			//getResult - Array
		}
		
	}
	public function tambahCart()
	{
		if(session('email_login') !== null)
		{
			$idBarang = $_POST['barang'];
			$jumlah = $_POST['jumlah'];
			$balasan = "sukses";
			$cart = new cart();
			$barang = new barang();
			$qtyTersedia = $barang -> getData($idBarang) -> getRow();
			if($qtyTersedia->statusBarang == 'Ready')
			{
				$qtyTersedia = $qtyTersedia->stokBarang;
				if($cart -> checkCart(session('email_login'),$idBarang) == 0)
				{
					if($jumlah > $qtyTersedia) $balasan = 'stok tidak cukup';
					else $cart -> insertData(session('email_login'),$idBarang,$jumlah);
				}
				else
				{
					$qtyCart = $cart -> getOneCart(session('email_login'),$idBarang) -> getRow()->kuantitas;
					if($qtyCart + $jumlah > $qtyTersedia) $balasan = 'stok tidak cukup';
					else $cart -> updateQty(session('email_login'),$idBarang,$jumlah);
				}
				return json_encode(['pesan' => $balasan, 'kode' => 200]);
			}
			else return json_encode(['pesan' => 'Maaf saat ini Barang belum ready', 'kode' => 400]);
		}
		else 
		{
			return json_encode(['pesan' => 'Silahkan Login untuk menambahkan Barang', 'kode' => 420]);
		}
		
		
	}
	public function adminBarang()
	{
		$validated = $this->validate([
            'file' => [
                'uploaded[file]',
                'mime_in[file,image/jpg,image/jpeg,image/gif,image/png]',
                'max_size[file,4096]',
            ],
        ]);
		$msg = 'Please select a valid file';
		//
		$Barang = new Barang();
		$nama = $_POST['nama'];
		$harga = $_POST['harga'];
		$deskripsi = $_POST['deskripsi'];
		$stok = $_POST['stok'];
		$status = $_POST['status'];
		$genre = "";
		$kategori = $_POST['kategori'][0];
		$berat = (int)$_POST['berat'];
		$ctr = 0;
		if(isset($_POST['genre']))
		{
			foreach($_POST['genre'] as $row)
			{
				if($ctr == 0) $genre = $genre . $row;
				else $genre = $genre . " - " .  $row;
				$ctr++;
			}
		}
		$tgl = $_POST['tglrilis'];
		//var_dump($berat);
		//tipebarang -> idBarang
		$tipeBarang = count($Barang -> getCode(substr($kategori,0,1)) -> getResultArray()) + 1;
		$tipeBarang = substr($kategori,0,1).str_pad((string)$tipeBarang ,4,"0",STR_PAD_LEFT);
		$file = $this->request->getFile('gambar');
		$file->move(WRITEPATH . '../public/img/product',$tipeBarang.'.jpg');
		$Barang -> insertData($nama,$harga,$tipeBarang,$deskripsi,$stok,$status,$kategori,$tgl,$genre,(int)$berat);
		return redirect()->to(site_url('public/masterinsert'));
	}
	public function logout()
	{
		session()->remove('email_login');
		return redirect()->to(base_url('public/login'));
	}

	public function profile(){
		if(isset(session()->email_login)){
			$userlogin = session()->email_login;
			$user = new userModel();
			$hasil = $user-> getDatabyEmail($userlogin)->getRowArray();
			echo view('header');
			echo view('profile',$hasil);
			echo view('footer.php');
		}
		else{
			return redirect()->to(site_url('public/login'));
		}
		//var_dump($hasil['namaUser']);
	}

	public function editprofile(){
		$email = $_POST['email'];
		$nama = $_POST['nama'];
		$oldpass = md5($_POST['oldpass']);
		$newpass = $_POST['npass'];
		$cpass = $_POST['cpass'];
		$user = new UserModel();
		$hasil = $user->updateData($email,$nama);
		if($hasil == 1){
			session()->setFlashdata('berhasil', 'Berhasil Update');
		}
		else{
			session()->setFlashdata('gagal', 'Gagal Update');
		}
		if($oldpass!=null && $newpass != null && $cpass != null){
			$hasil2 = $user->getData($email,$oldpass)->getRowArray();
			if($hasil2 != 0){
				if($cpass == $newpass){
					//update password
					$hasil3 = $user->updatePass($email,md5($newpass));
					if($hasil3 == 1){
						session()->setFlashdata('berhasil2', 'Berhasil Update');
					}
					else{
						session()->setFlashdata('gagal2', 'Gagal Update');
					}
				}
				else{
					session()->setFlashdata('gagal2', 'Konfirmasi Password Tidak Sesuai');
				}
			}
			else{
				session()->setFlashdata('gagal2', 'Password Lama Tidak Sesuai');
			}
		}
		return redirect()->to(site_url('public/profile'));
	}

	public function editAlamat(){
		$data['email'] = $_POST['email'];
		$data['idAlamat'] = "";
		$data['namaJalan'] = $_POST['alamat'];
		$data['kodePos'] = $_POST['kodePos'];
		$data['provinsi'] =$_POST['provinsi'];
		$data['kota'] = $_POST['nama_kota'];
		$data['id_kota'] = $_POST['kota'];
		$data['kecamatan'] = $_POST['kecamatan'];
		$data['telpon'] = $_POST['telp'];
		$idAlamat = $_POST['idAlamat'];
		if($idAlamat==null){
			$idAlamat=0;
		}
		$alamat = new AlamatModel();
		$hasil1 = $alamat->getData($data['email'])->getRowArray();
		if($hasil1==null){
			//insert
			if($this->form_validation->run($data, 'editAlamat') == FALSE)
			{
				session()->setFlashdata('errors', $this->form_validation->getErrors());
				return redirect()->to(site_url('public/alamat'));
			} 
			else{
				$hasil = $alamat->insertData($data['email'],$data['idAlamat'],$data['namaJalan'],
				$data['kodePos'],$data['provinsi'],$data['kota'],$data['id_kota'],
				$data['kecamatan'],$data['telpon']);
				if($hasil->getRowArray()){
					session()->setFlashdata('sukses','Berhasil Update');
				}
				else{
					session()->setFlashdata('error1','Gagal Update');
				}
			}
		}
		else{
			//update
			if($this->form_validation->run($data, 'editAlamat') == FALSE)
			{
				session()->setFlashdata('errors', $this->form_validation->getErrors());
				return redirect()->to(site_url('public/alamat'));
			} 
			else{
				$alamat = new AlamatModel();
				$hasil = $alamat->updateData($data['email'],$data['namaJalan'],
				$data['kodePos'],$data['provinsi'],$data['kota'],$data['id_kota'],
				$data['kecamatan'],$data['telpon']);
				if($hasil->getRowArray()){
					session()->setFlashdata('sukses','Berhasil Update');
				}
				else{
					session()->setFlashdata('error1','Gagal Update');
				}
				//var_dump($data['idAlamat']);
				//var_dump($hasil->getRowArray());
			}
		}
		return redirect()->to(site_url('public/alamat'));
	}


	public function alamat(){
		if(isset(session()->email_login)){
			$userlogin = session()->email_login;
			$user = new userModel();
			$alamat = new AlamatModel();
			//$hasil['dataUser'] = $user-> getDatabyEmail($userlogin)->getRowArray();
			$hasil['dataAlamat'] = $alamat-> getData($userlogin)->getRowArray();
			if(!isset($hasil['dataAlamat'])){
				$hasil['dataAlamat']['namaJalan']="";
				$hasil['dataAlamat']['kodePos']="";
				$hasil['dataAlamat']['provinsi']="";
				$hasil['dataAlamat']['kota']="";
				$hasil['dataAlamat']['id_kota']=-1;
				$hasil['dataAlamat']['kecamatan']="";
				$hasil['dataAlamat']['telpon']="";
				$hasil['dataAlamat']['idAlamat']="";
			}
			$province = $this->rajaongkir('city');
			echo view('header');
			echo view('alamat',[
				'dataUser'=>$user-> getDatabyEmail($userlogin)->getRowArray(),
				'dataAlamat'=>$hasil['dataAlamat'],
				'provinsi'=>json_decode($province)->rajaongkir->results
			]);
			echo view('footer.php');
			//var_dump(json_decode($province)->rajaongkir);
			//var_dump($hasil['dataAlamat']);
		}
		else{
			return redirect()->to(site_url('public/login'));
		}
		//var_dump($hasil['namaUser']);
	}

	public function testMidtrans(){
		echo view("checkout_snap");
	}

	public function listKota(){
		// Ambil data ID Provinsi yang dikirim via ajax post
		$id_provinsi = $_POST['id_prov'];
		//$id_provinsi = 73;
		$kotaModel = new provinsiModel();
		$kota = $kotaModel->getKotabyProvinsi($id_provinsi)->getResultArray();
		$lists = "<option value=''>Pilih</option>";
		foreach($kota as $data){
		  $lists .= "<option value='".$data['id_kab']."'>".$data['nama']."</option>"; // Tambahkan tag option ke variabel $lists
		}
		$callback = array('list_kota'=>$lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota
		echo $callback;
		//var_dump($callback);
	  }

	  public function getCity(){
		  //if($this->request->isAJAX()){
			  $id_province = $this->request->getGet('id_province');
			  $data = $this->rajaongkir('city',$id_province);
			  return $this->response->setJSON($data);
		  //}
	  }

	  public function rajaongkir($method, $id_province=null){
		$endPoint = $this->url.$method;

		if($id_province!=null)
		{
			$endPoint = $endPoint."?province=".$id_province;
		}
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $endPoint,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
			"key:".$this->api_key
		  ),
		));
		
		$response = curl_exec($curl);
		$err = curl_error($curl);
		
		curl_close($curl);
		
		return $response;
	}

	public function ongkir($tujuan,$berat){
		$curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://api.rajaongkir.com/starter/cost",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => "origin=254&destination=".$tujuan."&weight=".$berat."&courier=jne",
		CURLOPT_HTTPHEADER => array(
			"content-type: application/x-www-form-urlencoded",
			"key:".$this->api_key
		),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		return $response;
	}

	public function editBarang()
	{
		if(session('email_login') == 'admin')
		{
			helper('number');
			$data_b = new Barang();
			$data_kirim = [
				'page' => (isset($_GET['page'])) ? $_GET['page'] : 1,
				'cari' => (isset($_GET['cari'])) ? $_GET['cari'] : "",
				'kategori' => (isset($_GET['kategori'])) ? $_GET['kategori'] : "",
				'minVal' => (isset($_GET['minVal'])) ? $_GET['minVal']: "",
				'maxVal' =>(isset($_GET['maxVal'])) ? $_GET['maxVal']: "",
				'genre' => (isset($_GET['genre'])) ? $_GET['genre']: "",
				'status' => '',
				'sort' => ''
			];
			$data = [
				'barang'=> $data_b->fetch_data($data_kirim),
				'page'=>$data_kirim['page'],
				'jumlah'=> $data_b->getJumlahData($data_kirim),
				'kategori'=>$data_b->getKategori(),
				'genre'=>$data_b->getGenre()
			];
			echo view('loadCssJs');
			echo view('headerAdmin');
			echo view('masterEdit.php',$data);
		}
		else return redirect()->to(site_url('public/login'));
		
	}
	public function updtBarang()
	{
		$Barang = new Barang();
		// $id = 'A0001';
		// $barang = 'SONY Dualshock 4 Wireless Controller';
		// $harga = 699000;
		// $desk = 'baru diganti';
		// $stok = 101;
		// $kategori = 'Accessories';
		// $genre = '';
		// $tgl = '';
		// $stat = 'Ready';
		$id = $_POST['barang'];
		$barang = $_POST['nama'];
		$harga = $_POST['harga'];
		$desk = $_POST['desk'];
		$stok = $_POST['stok'];
		$kategori = $_POST['kat'];
		$genre = $_POST['genre'];
		$stat = $_POST['status'];
		$berat = $_POST['berat'];
		$Barang -> updateData($id,$barang,$harga,$desk,$stok,$stat,$kategori,$genre,$berat);
		return json_encode(['kode' => 'bisa']);
	}

	public function toDetail()
	{
		$rekomendasi = new Barang();
		$barang = new Barang();
		$testi = new testimoni();
		$id = $_GET['id'];
		$data['pilihan'] = $barang -> getData($id) -> getRow();
		$data['testimoni'] = $testi -> getReview($id) -> getResult();
		$data['rekomendasi'] = $rekomendasi -> getRecomendation($data['pilihan']->kategoriBarang,4) -> getResult();
		$data['bintang'] = 0;
		$data['jumlahReview'] = $testi -> getCount($id);
		if($data['jumlahReview'] == 0)
		{
			$data['bintang'] = 0;
		}
		else 
		{
			foreach($data['testimoni'] as $row)
			{
				$data['bintang'] = $data['bintang'] + (int)$row->bintangRating;
			}
			$data['bintang'] = (int)round(($data['bintang'] / $data['jumlahReview']));
		}
		//print_r($data['testimoni']);
		echo view("header");
		echo view("shop-details",$data);
		echo view("footer");
	}
	public function cariBarang()
	{
		$kategori = $_POST['jenis'];
		$nama = $_POST['nama'];
		$bawah = (int) $_POST['bawah'];
		$atas = (int) $_POST['atas'];
		$barang = new Barang();
		$data['barang'] = $barang -> cariMaster($nama,$kategori,$bawah,$atas);
		echo view('tableMaster',$data);
	}
    public function snap()
    {
		//$midtrans = new Midtrans();
		//echo $midtrans->coba();
    	echo view('checkout_snap');
    }
    public function token()
    {
		$barang = new Barang();
		$cart = new cart();
		$emailLogin = session('email_login');
		$data['cart'] = $cart -> getCart($emailLogin) -> getResult();
		$data['total'] = 0;
		$data['promo'] = 0;
		$item_details = array();
		foreach($data['cart'] as $row){
			$dataBarang = $barang->getBarang($row->id);
			foreach($dataBarang->getResultArray() as $row1):
				$dataBaru = [
					'id' => $row->id,
					'price' => $row1['hargaBarang'],
					'quantity' => $row->qty,
					'name' => $row1['namaBarang']
				];
				array_push($item_details,$dataBaru);
				$data['total'] = $data['total'] + ($row->qty * $row1['hargaBarang']);
			endforeach;
		}
		$dataBaru =[
			'id' => 'Ongkir',
			'price' => session('ongkir'),//ini yang diganti
			'quantity' => 1,
			'name' => 'Ongkos Kirim'
		];
		$data['total'] = $data['total'] + session('ongkir');
		array_push($item_details,$dataBaru);
		$htrans = new HTrans();
		$dtrans = new DTrans();
		$countHt = (string)($htrans->countData() + 1);
		$id = "AT";
		if($countHt < 10){
			$id = $id . "000" . $countHt;
		}else if($countHt < 100){
			$id = $id . "00" . $countHt;
		}else if($countHt < 1000){
			$id = $id . "0" . $countHt;
		}
		$transaction_details = array(
			'order_id' => $id,
			'gross_amount' => $data['total']
		);
		/*$data_h = $htrans->getData($id);
		$data_d = $dtrans->getData($id);
		foreach($data_h->getResultArray() as $row):
			$transaction_details = array(
				'order_id' => $id,//nanti ubh jdi $id
				'gross_amount' => $row['totalPembayaran']
			);
		endforeach;
		foreach($data_d->getResultArray() as $row):
			$dataBarang = $barang->getNama($row['idBarang']);
			foreach($dataBarang->getResultArray() as $row1):
				$dataBaru = [
					'id' => $row['idBarang'],
					'price' => $row['hargaBarang'],
					'quantity' => $row['kuantitas'],
					'name' => $row1['namaBarang']
				];
				array_push($item_details,$dataBaru);
			endforeach;
		endforeach;*/
		// Required
		/*$transaction_details = array(
		  'order_id' => rand(),
		  'gross_amount' => 94000, // no decimal allowed for creditcard
		);*/

		// Optional
		/*$item1_details = array(
		  'id' => 'a1',
		  'price' => 18000,
		  'quantity' => 3,
		  'name' => "Apple"
		);

		// Optional
		$item2_details = array(
		  'id' => 'a2',
		  'price' => 20000,
		  'quantity' => 2,
		  'name' => "Orange"
		);

		// Optional
		$item_details = array ($item1_details, $item2_details);*/

		$user = new userModel();
		$alamat = new alamatModel();
		$namaUser = $user -> getNama($emailLogin) -> getRow()->namaUser;
		$alamatUser = $alamat-> getData($emailLogin);

		foreach($alamatUser->getResultArray() as $row):
			// Optional
			$shipping_address = array(
				'first_name'    => $namaUser,
				'address'       => $row['namaJalan'],
				'city'          => $row['kota'],
				'postal_code'   => $row['kodePos'],
				'phone'         => $row['telpon'],
				'country_code'  => 'IDN'
			);
		endforeach;

		session()->set('address','Manggis 90');
		// Optional
		$customer_details = array(
		  'first_name'    => $namaUser,
		  'email'         => $emailLogin,
		  'shipping_address' => $shipping_address
		);

		// Data yang akan dikirim untuk request redirect_url.
        $credit_card['secure'] = true;
        //ser save_card true to enable oneclick or 2click
        //$credit_card['save_card'] = true;

        $time = time();
        $custom_expiry = array(
            'start_time' => date("Y-m-d H:i:s O",$time),
            'unit' => 'minute', 
            'duration'  => 2
        );
        
        $transaction_data = array(
            'transaction_details'=> $transaction_details,
            'item_details'       => $item_details,
            'customer_details'   => $customer_details,
            'credit_card'        => $credit_card,
            'expiry'             => $custom_expiry
        );

		error_log(json_encode($transaction_data));
		$midtrans = new Midtrans();
		$snapToken = $midtrans->getSnapToken($transaction_data);
		error_log($snapToken);
		echo $snapToken;
    }

    public function finish()
    {
		$pembayaran = new Pembayaran();
		$hTrans = new HTrans();
		$dTrans = new DTrans();
		$alamat = new alamatModel();
		$emailLogin = session('email_login');
		$result = json_decode($_POST['result_data']);
		$dataKu = (object)$result;
		//$hTrans->updateTglTransaksi($data->transaction_time,$data->order_id);
		$hTrans->insert([
			'idTrans' => $dataKu->order_id,
			'emailUser' => $emailLogin,
			'tglPemesanan' => $dataKu->transaction_time,
			'statusPembayaran' => $dataKu->transaction_status,
			'totalPembayaran' => $dataKu->gross_amount,
			'alamatPengiriman' => $alamat -> getId($emailLogin) -> getRow() ->idAlamat,
			'noResi' => 'belum',
			'ongkir' => session('ongkir'),
			'vakun' => $dataKu->va_numbers[0]->va_number
		]);
		$barang = new Barang();
		$cart = new cart();
		$data['cart'] = $cart -> getCart($emailLogin) -> getResult();
		$data['total'] = 0;
		$data['promo'] = 0;
		$item_details = array();
		foreach($data['cart'] as $row){
			$dataBarang = $barang->getBarang($row->id);
			foreach($dataBarang->getResultArray() as $row1):
				$dTrans->insert([
					'idBarang' => $row->id,
					'idTrans' => $dataKu->order_id,
					'kuantitas' => $row->qty,
					'subTotal' => ($row->qty * $row1['hargaBarang']),
					'hargaBarang' => $row1['hargaBarang']
				]);
				$kurangiStok = $row1['stokBarang'] - $row->qty;
				$barang->updateStok($kurangiStok,$row->id);
			endforeach;
		}
		$pembayaran->insert([
			'idTransaksi' => $dataKu->transaction_id,
			'idTrans' => $dataKu->order_id,
			'status' => $dataKu->transaction_status
		]);
		$data['cart'] = $cart -> getUserCart($emailLogin) -> getResult();
		foreach($data['cart'] as $row){
			$cart->deleteCartUser($emailLogin);
		}
		return redirect()->to(site_url('public/history'));
	}
	public function toCart()
	{
		if(session('email_login') !== null)
		{
			unset($_SESSION['promo']);
			unset($_SESSION['kodePromo']);
			$cart = new cart();
			$data['cart'] = $cart -> getCart(session('email_login')) -> getResult();
			$data['total'] = 0;
			$data['promo'] = 0;
			$data['berat'] = 0;
			foreach($data['cart'] as $row)
			{
				if($row->statusBarang == 'Ready'){
					$data['total'] = $data['total'] + ($row->qty * $row->harga);
					$brt = (int)$row->beratBarang;
					$data['berat'] = $data['berat'] + ($row->qty * $brt);
				}
			}
			$alamat = new AlamatModel();
			$hasil['dataAlamat'] = $alamat-> getData(session('email_login'))->getRowArray();
			if(!isset($hasil['dataAlamat'])){
				session()->set('alamat',"tidak");
			}
			else{
				session()->set('alamat',"ada");
				$tujuan = $hasil['dataAlamat']['id_kota'];
				//$berat = 100; //ambil dari atas nanti pas foreach
				$berat = $data['berat'];
				if($berat != 0){
					$dataongkir = $this->ongkir($tujuan,$berat);
					$ongkir = json_decode($dataongkir)->rajaongkir->results;
					$hargaongkir = $ongkir[0]->costs[1]->cost[0]->value;
					session()->set('ongkir',$hargaongkir);
				}
				//var_dump($ongkir[0]->costs[1]->cost[0]->value);
			}
			//var_dump($berat);
			session()->set('hargaCart',$data['total']);
			echo view('header');
			echo view('shopping-cart',$data);
			echo view('footer');
		}
		else{
			redirect()->to(site_url('public/login'));
		}
		
	}
	
	public function hapusCart()
	{
		$idBarang = $_POST['barang'];
		//$idBarang = 'A0001';
		$diskon = 0;
		$cart = new cart();
		$cart -> deleteCart(session('email_login'),$idBarang);
		$data['cart'] = $cart -> getCart(session('email_login')) -> getResult();
		$data['total'] = 0;
		$data['berat'] =0;
		foreach($data['cart'] as $row)
		{
			if($row->statusBarang == 'Ready')
			$data['total'] = $data['total'] + ($row->qty * $row->harga);
			$data['berat'] = $data['berat'] + ($row->qty * $row->beratBarang);
		}
		session()->set('hargaCart',$data['total']);
		//
		if(isset($_SESSION['promo']))
		{
			if($_SESSION['promo'] < 100)
			{
				$diskon = session('hargaCart')/session('promo');
			}
			else
			{
				$diskon = session('promo');
			}
			$kurang = session('hargaCart') - $diskon;
			session()->set('hargaCart',$kurang);
		}
		$hargaongkir = 0;
		if(session('alamat')=="ada"){
			//update ongkir
			$alamat = new AlamatModel();
			$hasil['dataAlamat'] = $alamat-> getData(session('email_login'))->getRowArray();
			if(!isset($hasil['dataAlamat'])){
				session()->set('alamat',"tidak");
			}
			else{
				session()->set('alamat',"ada");
				$tujuan = $hasil['dataAlamat']['id_kota'];
				//$berat = 100; //ambil dari atas nanti pas foreach
				$berat = $data['berat'];
				$dataongkir = $this->ongkir($tujuan,$berat);
				$ongkir = json_decode($dataongkir)->rajaongkir->results;
				$hargaongkir = $ongkir[0]->costs[1]->cost[0]->value;
				session()->set('ongkir',$hargaongkir);
				//var_dump($ongkir[0]->costs[1]->cost[0]->value);
			}
			// //
		}
		
		return json_encode(['kode' => 'Pengahpusan berhasil','total' => session('hargaCart'),'ongkir'=>$hargaongkir,'diskon' => $diskon,'subtotal' => $data['total']]);
	}
	public function ubahCart()
	{
		$idBarang = $_POST['barang'];
		$jumlah = $_POST['jumlah'];
		$balasan = "sukses";
		$diskon = 0;
		$cart = new cart();
		$barang = new barang();
		$qtyTersedia = $barang -> getData($idBarang) -> getRow();
		$qtyTersedia = $qtyTersedia->stokBarang;
		if($jumlah > $qtyTersedia)
		{
			$diskon = (session('promo') !== null)?session('promo'):0;
			return json_encode(['pesan' => 'Gagal! Stok tidak mencukupi','diskon' => $diskon,'total' => session('hargaCart'),'kode' => 400,'subtotal' => $diskon+session('hargaCart')]);
		} 
		else
		{
			$cart -> updateCart(session('email_login'),$idBarang,$jumlah);
			//cek ulang harga
			$cart = new cart();
			$data['cart'] = $cart -> getCart(session('email_login')) -> getResult();
			$data['total'] = 0;
			$data['berat'] = 0;
			foreach($data['cart'] as $row)
			{
				if($row->statusBarang == 'Ready')
				$data['total'] = $data['total'] + ($row->qty * $row->harga);
				$data['berat'] = $data['berat'] + ($row->qty * $row->beratBarang);
			}
			session()->set('hargaCart',$data['total']);

			$hargaongkir = 0;
			if(session('alamat')=="ada"){
				//update ongkir
				$alamat = new AlamatModel();
				$hasil['dataAlamat'] = $alamat-> getData(session('email_login'))->getRowArray();
				if(!isset($hasil['dataAlamat'])){
					session()->set('alamat',"tidak");
				}
				else{
					session()->set('alamat',"ada");
					$tujuan = $hasil['dataAlamat']['id_kota'];
					//$berat = 100; //ambil dari atas nanti pas foreach
					$berat = $data['berat'];
					$dataongkir = $this->ongkir($tujuan,$berat);
					$ongkir = json_decode($dataongkir)->rajaongkir->results;
					$hargaongkir = $ongkir[0]->costs[1]->cost[0]->value;
					session()->set('ongkir',$hargaongkir);
					//var_dump($ongkir[0]->costs[1]->cost[0]->value);
				}
				//
			}
			
			if(isset($_SESSION['promo']))
			{
				if($_SESSION['promo'] < 100)
				{
					$diskon = session('hargaCart')/session('promo');
				}
				else
				{
					$diskon = session('promo');
				}
				$kurang = session('hargaCart') - $diskon;
				session()->set('hargaCart',$kurang);
			}
			return json_encode(['pesan' => 'Berhasil mengubah kuantitas','diskon' => $diskon,'ongkir'=>$hargaongkir,'total' => session('hargaCart'),'kode' => 200,'subtotal' => $data['total']]);
		} 
	}
	public function pakaiPromo()
	{
		$kode = $_POST['promo'];
		$promo = new promo();
		$promo = $promo -> getPromo($kode) -> getRow();
		$diskon = 0;
		//cek ulang harga
		$cart = new cart();
		$data['cart'] = $cart -> getCart(session('email_login')) -> getResult();
		$data['total'] = 0;
		foreach($data['cart'] as $row)
		{
			if($row->statusBarang == 'Ready')
			$data['total'] = $data['total'] + ($row->qty * $row->harga);
		}
		session()->set('hargaCart',$data['total']);
		//
		if(isset($promo->diskon))
		{
			session()->set('promo', $promo->diskon);
			session()->set('kodePromo', $kode);
			if($promo->diskon < 100)
			{
				$diskon = session('hargaCart')/$promo->diskon;
			}
			else
			{
				$diskon = $promo->diskon;
			}
			$temp = session('hargaCart') - $diskon;
			session()->set('hargaCart',$temp);
			return json_encode(['pesan' => 'Kode promo berhasil digunakan','diskon' => $diskon,'total' => session('hargaCart'),'kode' => 200,'subtotal' => $data['total']]);
		}
		else
		{
			return json_encode(['pesan' => 'Kode promo tidak ditemukan','diskon' => 0,'total' => session('hargaCart'), 'kode' => 420]);
		}
	}

	public function history(){
		$email = session()->email_login;
		$user = new userModel;
		if(isset($email)){
			$htrans = new HTrans();
			$dtrans = new DTrans();
			$barang = new Barang();
			$pembayaran = new Pembayaran();
			$midtrans = new Midtrans();
			$data['htransCek'] = $htrans->getDataByEmail($email)->getResult();
			foreach($data['htransCek'] as $row)
			{
				$id = $pembayaran->getData($row->idTrans)->getRow()->idTransaksi;
				if($row->statusPembayaran == "pending"){
					$statusPembayaran = $midtrans->status($id);
					$dataKu = (object)$statusPembayaran;
					if($dataKu->transaction_status == "settlement"){
						$htrans->updateStatus($row->idTrans,'settlement');
					}
					if($dataKu->transaction_status == "expire"){
						$dataDtrans = $dtrans -> getData($row->idTrans) ->getResult();
						foreach($dataDtrans as $row){
							$stokBarang = $barang->getData($row->idBarang)->getRow()->stokBarang;
							$stokBarang = $stokBarang + $row->kuantitas;
							$barang->updateStok($stokBarang,$row->idBarang);
						}
						$htrans->updateStatus($row->idTrans,'expired');
					}
				}
			}
			$data['htrans'] = $htrans->getDataByEmail($email)->getResultArray();
			if($data['htrans']==null){
				$data['htrans'] = 0;
			}
			$data['dataUser']= $user-> getDatabyEmail($email)->getRowArray();
			echo view('header');
			echo view('history',$data);
			echo view('footer.php');
		}
		else{
			return redirect()->to(base_url('/public/login'));
		}
	}	

	public function orderDetails(){
		//halaman detail order dari history
		$email = session()->email_login;
		if(isset($email)){
			$user = new userModel;
			$data['dataUser']= $user-> getDatabyEmail($email)->getRowArray();
			$id = $_GET['id'];
			$htrans = new hTrans();
			$data['htrans'] = $htrans->getData($id)->getRowArray();
			$dtrans = new dTrans();
			$data['dtrans'] = $dtrans->getDataDetail($id)->getResultArray();
			//var_dump($data['htrans']);
			//Bagian Tracking
			$data['pengiriman'] = 'sudah';
			$noResi = $htrans->getData($id)->getRow()->noResi;
			if($noResi == 'belum') $data['pengiriman'] = 'belum';
			else
			{
				$courier = 'sicepat';
				$url = "https://api.binderbyte.com/v1/track?api_key=7347e7ced4b8d0c0a822ee1fd6a5ca0b35a0378d6b46cb0ca8a7cbe42791f065&courier=$courier&awb=$noResi";
				$ch = curl_init();

				//atur URL
				curl_setopt($ch,CURLOPT_URL,$url);
				//atur kembalian
				curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
				$output = curl_exec($ch);
				if(!$output) echo "ERROR : " . curl_error($ch);
				curl_close($ch);
				$output = json_decode($output, true);
				//echo $output['data']['summary']['status']; cek status DELIVERED DLL
				$data['tracking'] = $output['data'];
				//
			}
			echo view('header');
			echo view('order-details',$data);
			echo view('footer.php');
		}
		else{
			return redirect()->to(base_url('/public/login'));
		}
	}	
	public function adminPesanan()
	{
		if(session('email_login') == 'admin')
		{
			$htrans = new HTrans();
			$dtrans = new DTrans();
			$barang = new Barang();
			$pembayaran = new Pembayaran();
			$midtrans = new Midtrans();
			$data['htransCek'] = $htrans->getBelum()->getResult();
			$data['htrans'] = array();
			foreach($data['htransCek'] as $row)
			{
				if($htrans->cekStatus($row->id)->getRow()->statusPembayaran == "pending"){
					$id = $pembayaran->getData($row->id)->getRow()->idTransaksi;
					$statusPembayaran = $midtrans->status($id);
					$dataKu = (object)$statusPembayaran;
					$htrans->updateStatus($row->id,$dataKu->transaction_status);
					if($dataKu->transaction_status == "settlement"){
						array_push($data['htrans'],$row);
					}else if($dataKu->transaction_status == "expire"){
						$dataDtrans = $dtrans->getData($row->id)->getResult();
						foreach($dataDtrans as $row1){
							$stokBarang = $barang->getData($row1->idBarang)->getRow()->stokBarang;
							$stokBarang = $stokBarang + $row1->kuantitas;
							$barang->updateStok($stokBarang,$row1->idBarang);
						}
						$htrans->updateStatus($row->id,'expired');
					}
				}else if($htrans->cekStatus($row->id)->getRow()->statusPembayaran == "settlement"){
					array_push($data['htrans'],$row);
				}
			}
			echo view('loadCssJs');
			echo view('headerAdmin');
			echo view('adminPesanan',$data);
		}
		else return redirect()->to(base_url('/public/login'));;
		
	}
	public function updateResi()
	{
		$idHtrans = $_POST['id'];
		$resi = $_POST['resi'];
		$htrans = new hTrans();
		$temp = $htrans->updateResi($idHtrans,$resi);
		return json_encode(['pesan' => "update Transaksi $idHtrans berhasil", 'kode' => 200]);
	}
	public function detailAdminPesanan()
	{
		$id = $_GET['id'];
		$dtrans = new dTrans();
		$data['detail'] = $dtrans -> getDataDetail($id) -> getResult();
		echo view('loadCssJs');
		echo view('headerAdmin');
		echo view('adminDtrans',$data);
	}
}